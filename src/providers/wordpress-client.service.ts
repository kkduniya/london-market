import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/concatMap';
import { Subscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Observable";

import { APP_CONFIG, AppConfig } from "../app/app.config";
import { NetworkError } from "../models/network-error.model";
import { Category } from "../models/category.models";
import { AuthResponse } from "../models/auth-response.models";
import { AuthCredential } from "../models/auth-credential.models";
import { RegisterRequest } from "../models/register-request.models";
import { RegisterResponse } from "../models/register-response.models";
import { UserResponse } from "../models/user-response.models";
import { Product } from "../models/product.models";
import { Review } from "../models/review.models";
import { PaymentGateway } from "../models/payment-gateway.models";
import { OrderRequest } from "../models/order-request.models";
import { OrderResponse } from "../models/order-response.models";
import { Order } from "../models/order.models";
import { ShippingLine } from "../models/shipping-line.models";
import { Currency } from "../models/currency.models";
import { Feedback } from "../models/feedback.models";
import { OrderUpdateRequest } from "../models/order-update-request.models";
import { Banner } from '../models/banner.models';
import { ResetPasswordResponse } from '../models/reset-password-response.models';
import { Coupon } from '../models/coupon.models';
import { Country } from '../models/country.models';
import { SocialToken } from '../models/social-token.models';
import { TimeSlot } from '../models/time-slot.models';
import { ShippingZone } from '../models/shipping-zone.models';
import { ShippingZoneLocation } from '../models/shipping-zone-location.models';
import { ShippingMethod } from '../models/shipping-method.models';

@Injectable()
export class WordpressClient {

	constructor(@Inject(APP_CONFIG) private config: AppConfig, private http: HttpClient) {

	}

	public getCountries(): Observable<Array<Country>> {
		// return this.http
		// .get<Array<Country>>('https://restcountries.eu/rest/v2/all')
		return this.http.get<Array<Country>>('./assets/json/countries.json').concatMap((data) => {
			let indiaObj: any = {};
			for (let index = 0; index < data.length; index++) {
				if (!(data[index].callingCodes.length) || data[index].callingCodes[0] == "") {
					data.splice(index, 1);
				}
				if (data[index].alpha2Code === "IN") {
					indiaObj = data[index];
					data.splice(index, 1);
				}
			}
			data.splice(0, 0, indiaObj);
			return Observable.of(data);
		});
	}

	public checkToken(adminToken, idToken: any): Observable<SocialToken> {
		const data = this.convertToParams({ token: idToken })
		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				// 'Content-Type':  'application/x-www-form-urlencoded',
				'Authorization': adminToken
			})
		}
		let token = this.http.post<SocialToken>(this.config.apiBase + 'mobile-ecommerce/v1/jwt/token/firebase', { token: idToken }, httpOptions);
		return token.concatMap(data => {
			return Observable.of(data);
		});
	}

	public timeslots(): Observable<Array<TimeSlot>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
		return this.http.get<Array<TimeSlot>>(this.config.apiBase + 'laundry/v1/slots/list', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}
	/*kk code for get page content*/
	public getPage(adminToken: string, pageId: string): Observable<UserResponse> {
		  let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
		  let link = 'contentbypageid' + pageId;
	      lastCacheTime  = localStorage.getItem('pageCacheTime');
	      timepast = (currentTime - lastCacheTime);
	      let pagecontent =JSON.parse(localStorage.getItem(link));
	      if(lastCacheTime !== null && timepast < 1800 && pagecontent !== null ){
	       var data = JSON.parse(localStorage.getItem(link));
	        return Observable.of(data);
	      }
	      else{
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get<UserResponse>(this.config.apiBase + 'wp/v2/pages/' + pageId, { headers: myHeaders }).concatMap(data => {
				window.localStorage.setItem("pageCacheTime", JSON.stringify(currentTime));/*set orders in localstorage*/
	          	window.localStorage.setItem('contentbypageid' + pageId, JSON.stringify(data));
				return Observable.of(data);
			});
		}
	}
	/*end*/
	public getUser(adminToken: string, userId: string): Observable<UserResponse> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<UserResponse>(this.config.apiBase + 'wc/v2/customers/' + userId, { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public updateUser(adminToken: string, userId: string, userUpdateRequest: any): Observable<UserResponse> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.put<UserResponse>(this.config.apiBase + 'wc/v2/customers/' + userId, JSON.stringify(userUpdateRequest), { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public createUser(adminToken: string, credentials: RegisterRequest): Observable<RegisterResponse> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.post<RegisterResponse>(this.config.apiBase + 'wp/v2/users', JSON.stringify(credentials), { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public getAuthToken(credentials: AuthCredential): Observable<AuthResponse> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
		console.log(myHeaders);
		return this.http.post<AuthResponse>(this.config.apiBase + 'mobile-ecommerce/v1/jwt/token', JSON.stringify(credentials), { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public resetPassword(userName: string): Observable<ResetPasswordResponse> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
		return this.http.post<ResetPasswordResponse>(this.config.apiBase + 'mobile-ecommerce/v1/password/forgot', JSON.stringify({ user_login: userName }), { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public createOrder(adminToken: string, createOrder: OrderRequest): Observable<OrderResponse> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.post<OrderResponse>(this.config.apiBase + 'wc/v2/orders/', JSON.stringify(createOrder), { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public myOrders(adminToken: string, customer_id: string, pageNo: string): Observable<Array<Order>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Order>>(this.config.apiBase + 'wc/v2/orders/' + '?customer=' + customer_id + '&page=' + pageNo, { headers: myHeaders }).concatMap(data => {
			for (let i = 0; i < data.length; i++) {
				let order = data[i];
				order.date_created = this.formatDate(new Date(order.date_created));
			}
			return Observable.of(data);
		});
	}

	public updateOrder(adminToken: string, orderId: string, orderUpdateRequest: any): Observable<Order> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.put<Order>(this.config.apiBase + 'wc/v2/orders/' + orderId, JSON.stringify(orderUpdateRequest), { headers: myHeaders }).concatMap(data => {
			let order = data;
			order.date_created = this.formatDate(new Date(order.date_created));
			return Observable.of(data);
		});
	}


	public shippingLines(adminToken: string): Observable<Array<ShippingLine>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<ShippingLine>>(this.config.apiBase + 'wc/v2/shipping_methods/', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public getCouponByCode(adminToken: string, cCode: string): Observable<Array<Coupon>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Coupon>>(this.config.apiBase + 'wc/v2/coupons?code=' + cCode, { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public applyCouponCode(adminToken: string, orderId: string, cCode: string): Observable<Order> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Order>(this.config.apiBase + 'mobile-ecommerce/v1/coupon/order/' + orderId + '/apply-coupon?code=' + cCode, { headers: myHeaders }).concatMap(data => {
			let order = data;
			order.date_created = this.formatDate(new Date(order.date_created));
			return Observable.of(data);
		});
	}

	public categoriesParent(adminToken: string): Observable<Array<Category>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		console.log(myHeaders);
		return this.http.get<Array<Category>>(this.config.apiBase + 'wc/v2/products/categories/?per_page=25&parent=0&_embed', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public categories(adminToken: string, pageNo: string): Observable<Array<Category>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Category>>(this.config.apiBase + 'wc/v2/products/categories/?per_page=90&page=' + pageNo + '&_embed', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public productVariations(adminToken: string, productId: string): Observable<Array<Product>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/' + productId + '/variations/', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public productsReviews(adminToken: string, productId: string): Observable<Array<Review>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Review>>(this.config.apiBase + 'wc/v2/products/' + productId + '/reviews/', { headers: myHeaders }).concatMap(data => {
			for (let i = 0; i < data.length; i++) {
				let review = data[i];
				review.date_created = this.formatDate(new Date(review.date_created));
			}
			return Observable.of(data);
		});
	}

	public banners(adminToken: string): Observable<Array<Banner>> {
		let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
	      lastCacheTime  = localStorage.getItem("BannerCacheTime");
	      timepast = (currentTime - lastCacheTime);
	      let banners =JSON.parse(localStorage.getItem("banners"));
	      if(lastCacheTime !== null && timepast < 1800 && banners !== null ){
	       var data = JSON.parse(localStorage.getItem("banners"));
	        return Observable.of(data);
	      }
	      else{
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get<Array<Banner>>(this.config.apiBase + 'mobile-ecommerce/v1/banners/list', { headers: myHeaders }).concatMap(data => {
				return Observable.of(data);
			});
		 }
	}

	public productsAll(adminToken: string, pageNo: string): Observable<Array<Product>> {
		  let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
		  console.log(currentTime);
		  let link = 'allproducts' + pageNo;
	      lastCacheTime  = localStorage.getItem("productallCacheTime");
	      timepast = (currentTime - lastCacheTime);
	      console.log(lastCacheTime, currentTime, timepast);
	      let productdata =JSON.parse(localStorage.getItem(link));
	      if(lastCacheTime !== null && timepast < 100 && productdata !== null ){
	       var data = JSON.parse(localStorage.getItem(link));
	        return Observable.of(data);
	      }
	      else{ 
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/' + '?per_page=20&page=' + pageNo + '&order=desc&orderby=date' + '&_embed', { headers: myHeaders }).concatMap(data => {
				window.localStorage.setItem("productallCacheTime", JSON.stringify(currentTime));/*set orders in localstorage*/
	          	window.localStorage.setItem("allproducts"+pageNo, JSON.stringify(data));
				return Observable.of(data);
			});
		}
	}

	/*for New Brand A-Z All without per_page */
	public productsAll_A_Z(adminToken: string, pageNo: string): Observable<Array<Product>> {
	  let currentTime:any;
	  let lastCacheTime:any;
	  let timepast:any;
	  currentTime = new Date().getTime() / 1000;
      lastCacheTime  = localStorage.getItem("productCacheTimea2z");
      timepast = (currentTime - lastCacheTime);
      let productdata =JSON.parse(localStorage.getItem("allproductsa2z"));
      if(lastCacheTime !== null && timepast < 1800 && productdata !== null ){
       var data = JSON.parse(localStorage.getItem("allproductsa2z"));
        return Observable.of(data);
      }
      else{
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/' + '?per_page=100&page=1&order=asc&orderby=title' + '&_embed', { headers: myHeaders }).concatMap(data => {
			window.localStorage.setItem("productCacheTimea2z", JSON.stringify(currentTime));/*set orders in localstorage*/
          	window.localStorage.setItem("allproductsa2z", JSON.stringify(data));
			return Observable.of(data);
		});
	 }
	}
	/*parents sub category*/
	public subcategoryofparent(adminToken: string, catId: string): Observable<Array<Category>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Category>>(this.config.apiBase + 'wc/v2/products/categories/?order=desc&orderby=count&parent='+catId+'&_embed', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	/*get all products tag*/
	public productstag(adminToken: string): Observable<Array<Product>> {
	  let currentTime:any;
	  let lastCacheTime:any;
	  let timepast:any;
	  currentTime = new Date().getTime() / 1000;
      lastCacheTime  = localStorage.getItem("alltagschache");
      timepast = (currentTime - lastCacheTime);
      let product_tag =JSON.parse(localStorage.getItem("producttags"));
      if(lastCacheTime !== null && timepast < 1800 && product_tag !== null ){
       var data = JSON.parse(localStorage.getItem("producttags"));
        return Observable.of(data);
      }
      else{
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/tags' + '?per_page=80&page=1&orderby=slug&order=asc' + '&_embed', { headers: myHeaders }).concatMap(data => {
			window.localStorage.setItem("alltagschache", JSON.stringify(currentTime));/*set orders in localstorage*/
          	window.localStorage.setItem("producttags", JSON.stringify(data));
			return Observable.of(data);
		});
	 }
	}

	/*Products by Tags*/
	public productsByTags(adminToken: string, tagId: string, pageNo: string): Observable<Array<Product>> {
		  let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
		  let link = 'productsByTags' + tagId + pageNo;
	      lastCacheTime  = localStorage.getItem('pbytagsCacheTime');
	      timepast = (currentTime - lastCacheTime);
	      let pbycat =JSON.parse(localStorage.getItem(link));
	      if(lastCacheTime !== null && timepast < 1800 && pbycat !== null ){
	       var data = JSON.parse(localStorage.getItem(link));
	        return Observable.of(data);
	      }
	      else{
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/' + '?tag=' + tagId + '&per_page=20&page=' + pageNo + '&_embed', { headers: myHeaders }).concatMap(data => {
				window.localStorage.setItem("pbytagsCacheTime", JSON.stringify(currentTime));/*set orders in localstorage*/
	          	window.localStorage.setItem('productsByTags' + tagId + pageNo, JSON.stringify(data));
				return Observable.of(data);
			});
	    }
	}

	public productsByTagsRefresh(adminToken: string, tagId: string, pageNo: string): Observable<Array<Product>> {
	  	let link = 'productsByTags' + tagId + pageNo;
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/' + '?tag=' + tagId + '&per_page=20&page=' + pageNo + '&_embed', { headers: myHeaders }).concatMap(data => {
          	window.localStorage.setItem('productsByTags' + tagId + pageNo, JSON.stringify(data));
			return Observable.of(data);
		});  
	}
	/*end*/
	public productById(adminToken: string, proId: string): Observable<Product> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Product>(this.config.apiBase + 'wc/v2/products/' + proId, { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public productsByQuery(adminToken: string, query: string, pageNo: string): Observable<Array<Product>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/' + '?search=' + query + '&per_page=20&page=' + pageNo + '&_embed', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public productsByCategory(adminToken: string, catId: string, pageNo: string): Observable<Array<Product>> {
		  let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
		  let link = 'productsByCategory' + catId + pageNo;
	      lastCacheTime  = localStorage.getItem('pbycatCacheTime');
	      timepast = (currentTime - lastCacheTime);
	      let pbycat =JSON.parse(localStorage.getItem(link));
	      if(lastCacheTime !== null && timepast < 1800 && pbycat !== null ){
	       var data = JSON.parse(localStorage.getItem(link));
	        return Observable.of(data);
	      }
	      else{
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/' + '?category=' + catId + '&per_page=20&page=' + pageNo + '&_embed', { headers: myHeaders }).concatMap(data => {
				window.localStorage.setItem("pbycatCacheTime", JSON.stringify(currentTime));/*set orders in localstorage*/
	          	window.localStorage.setItem('productsByCategory' + catId + pageNo, JSON.stringify(data));
				return Observable.of(data);
			});
	    }
	}

	/*without cache*/
	public productsByCate_Cache(adminToken: string, catId: string, pageNo: string): Observable<Array<Product>> {
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/' + '?category=' + catId + '&per_page=20&page=' + pageNo + '&_embed', { headers: myHeaders }).concatMap(data => {
	          	window.localStorage.setItem('productsByCategory' + catId + pageNo, JSON.stringify(data));
				return Observable.of(data);
			});
	}
	/*end*/

	public currencies(adminToken: string): Observable<Currency> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http
			.get<Currency>(this.config.apiBase + 'wc/v2/settings/general/woocommerce_currency', { headers: myHeaders })
			.concatMap(data => {
				return Observable.of(data);
			});
	}

	public paymentGateways(adminToken: string): Observable<Array<PaymentGateway>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<PaymentGateway>>(this.config.apiBase + 'wc/v2/payment_gateways/', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public shippingZones(adminToken: string): Observable<Array<ShippingZone>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<ShippingZone>>(this.config.apiBase + 'wc/v2/shipping/zones/', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	public shippingZoneLocations(adminToken: string, zoneId: string): Observable<Array<ShippingZoneLocation>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<ShippingZoneLocation>>(this.config.apiBase + 'wc/v2/shipping/zones/' + zoneId + '/locations', { headers: myHeaders }).concatMap(data => {
			for (let zl of data) {
				zl.zoneId = zoneId;
			}
			return Observable.of(data);
		});
	}

	public shippingMethods(adminToken: string, zoneId: string): Observable<Array<ShippingMethod>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<ShippingMethod>>(this.config.apiBase + 'wc/v2/shipping/zones/' + zoneId + '/methods', { headers: myHeaders }).concatMap(data => {
			for (let sm of data) {
				if (sm.settings && sm.settings.cost && sm.settings.cost.value && sm.settings.cost.value.length) {
					sm.cost = Number(sm.settings.cost.value);
				} else if (sm.settings && sm.settings.min_amount && sm.settings.min_amount.value && sm.settings.min_amount.value.length) {
					sm.cost = Number(sm.settings.min_amount.value);
				} else {
					sm.cost = -1;
				}
			}
			return Observable.of(data);
		});
	}

	public shippingMethod(adminToken: string, methodId: string): Observable<ShippingMethod> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<ShippingMethod>(this.config.apiBase + 'wc/v2/shipping_methods/' + methodId, { headers: myHeaders }).concatMap(sm => {
			if (sm.settings && sm.settings.cost && sm.settings.cost.value && sm.settings.cost.value.length) {
				sm.cost = Number(sm.settings.cost.value);
			} else if (sm.settings && sm.settings.min_amount && sm.settings.min_amount.value && sm.settings.min_amount.value.length) {
				sm.cost = Number(sm.settings.min_amount.value);
			} else {
				sm.cost = -1;
			}
			return Observable.of(sm);
		});
	}

	convertToParams(data) {
		var p = [];
		for (var key in data) {
			p.push(key + '=' + encodeURIComponent(data[key]));
		}
		return p.join('&');
	}

	private formatDate(date: Date): string {
		let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
	}

	/*product by vendor id*/
	public productsByVendor(adminToken: string, vid: string, pageNo: string): Observable<Array<Product>> {
		let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
		  let link = 'productsByvendor' + vid + pageNo;
	      lastCacheTime  = localStorage.getItem('pbyvendorCacheTime');
	      timepast = (currentTime - lastCacheTime);
	      let pbycat =JSON.parse(localStorage.getItem(link));
	      if(lastCacheTime !== null && timepast < 1800 && pbycat !== null ){
	       var data = JSON.parse(localStorage.getItem(link));
	        return Observable.of(data);
	      }
	      else{
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/' + '?vendor=' + vid + '&per_page=20&page=' + pageNo + '&_embed', { headers: myHeaders }).concatMap(data => {
				window.localStorage.setItem("pbyvendorCacheTime", JSON.stringify(currentTime));/*set orders in localstorage*/
	          	window.localStorage.setItem('productsByvendor' + vid + pageNo, JSON.stringify(data));
				return Observable.of(data);
			});
	    }
	}

	/*get all vendors*/
	public getAllVendor(adminToken: string): Observable<Array<Product>> {
		let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
		  let link = 'allvendors';
	      lastCacheTime  = localStorage.getItem('allvendorsCacheTime');
	      timepast = (currentTime - lastCacheTime);
	      let pbycat =JSON.parse(localStorage.getItem(link));
	      if(lastCacheTime !== null && timepast < 1800 && pbycat !== null ){
	       var data = JSON.parse(localStorage.getItem(link));
	        return Observable.of(data);
	      }
	      else{
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get<Array<Product>>(this.config.apiBase + 'wcmp/v1/vendors', { headers: myHeaders }).concatMap(data => {
				window.localStorage.setItem("allvendorsCacheTime", JSON.stringify(currentTime));/*set orders in localstorage*/
	          	window.localStorage.setItem('allvendors', JSON.stringify(data));
				return Observable.of(data);
			});
	    }
	}

	public getAllVendorwocache(adminToken: string): Observable<Array<Product>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Product>>(this.config.apiBase + 'wcmp/v1/vendors', { headers: myHeaders }).concatMap(data => {
          	window.localStorage.setItem('allvendors', JSON.stringify(data));
			return Observable.of(data);
		});
	}
	/*end*/

	/*product by metadata for seasons shopping*/
	public productsByMetadata(adminToken: string, attribute, pageNo: string): Observable<Array<Product>> {
		  /*let attribute =[{"name":"Season","value": metadata}];*/
		  let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
		  let link = 'productsByMetadata' + attribute + pageNo;
	      lastCacheTime  = localStorage.getItem('pmetadataCacheTime');
	      timepast = (currentTime - lastCacheTime);
	      let pbymeta =JSON.parse(localStorage.getItem(link));
	      if(lastCacheTime !== null && timepast < 1800 && pbymeta !== null ){
	       var data = JSON.parse(localStorage.getItem(link));
	        return Observable.of(data);
	      }
	      else{
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v2/products/?attribute_term=' + attribute + '&per_page=20&page=' + pageNo + '&_embed', { headers: myHeaders }).concatMap(data => {
				window.localStorage.setItem("pmetadataCacheTime", JSON.stringify(currentTime));/*set orders in localstorage*/
	          	window.localStorage.setItem('productsByMetadata' + attribute + pageNo, JSON.stringify(data));
				return Observable.of(data);
			});
	    }
	}
	/*end*/

	/*Exhibitions*/
	public exhibitionsdata(adminToken: string) {
		let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
		  let link = 'exhibition';
	      lastCacheTime  = localStorage.getItem('exhibitionpageCacheTime');
	      timepast = (currentTime - lastCacheTime);
	      let pagecontent =JSON.parse(localStorage.getItem(link));
	      if(lastCacheTime !== null && timepast < 1000 && pagecontent !== null ){
	       var data = JSON.parse(localStorage.getItem(link));
	        return Observable.of(data);
	      }
	      else{
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http
			.get<Array<Product>>(this.config.apiBase + 'custom-api/v1/customposts', { headers: myHeaders })
			.concatMap(data => {
			window.localStorage.setItem("exhibitionpageCacheTime", JSON.stringify(currentTime));/*set orders in localstorage*/
	        window.localStorage.setItem('exhibition', JSON.stringify(data));
			return Observable.of(data);
		});

		}
	}
	/*end*/

	/*get all vendors*/
	public get_vendorby_id(adminToken: string, vid: string): Observable<Array<Product>>
	{
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Product>>(this.config.apiBase + 'wcmp/v1/vendors/'+vid, { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}
	/*end*/

	/*sorting products*/
	public productsByCategorySort(adminToken: string, catId: string, pageNo: string, sort: string): Observable<Array<Product>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v3/products/' + '?category=' + catId + '&orderby=price&order=' + sort + '&per_page=20&page=' + pageNo + '&_embed', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}
	/*end*/

	/*sorting products*/
	public insert_feedback(adminToken: string, feeddata): any {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.post(this.config.apiBase + 'customer_feedback/v1/add_feedback/?name=' +feeddata.name+'&email='+feeddata.email+'&mobile='+feeddata.mobile+'&description='+feeddata.info, { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}
	/*end*/

	/*sorting products*/
	public get_attributes(adminToken: string): any {
		  let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
		  let link = 'attributes';
	      lastCacheTime  = localStorage.getItem('attributesCacheTime');
	      timepast = (currentTime - lastCacheTime);
	      let pagecontent =JSON.parse(localStorage.getItem(link));
	      if(lastCacheTime !== null && timepast < 1800 && pagecontent !== null ){
	       var data = JSON.parse(localStorage.getItem(link));
	        return Observable.of(data);
	      }
	      else
	      {
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get(this.config.apiBase + 'wc/v3/products/attributes', { headers: myHeaders }).concatMap(data => {
				window.localStorage.setItem("attributesCacheTime", JSON.stringify(currentTime));/*set orders in localstorage*/
	        	window.localStorage.setItem('attributes', JSON.stringify(data));
				return Observable.of(data);
			});
		}
	}
	/*end*/

	/*sorting products*/
	public get_attributes_term(adminToken: string, term:string): any {
		  let currentTime:any;
		  let lastCacheTime:any;
		  let timepast:any;
		  currentTime = new Date().getTime() / 1000;
		  let link = 'attributesterms'+term;
	      lastCacheTime  = localStorage.getItem('attributestermsCacheTime');
	      timepast = (currentTime - lastCacheTime);
	      let pagecontent =JSON.parse(localStorage.getItem(link));
	      if(lastCacheTime !== null && timepast < 1800 && pagecontent !== null ){
	       var data = JSON.parse(localStorage.getItem(link));
	        return Observable.of(data);
	      }
	      else
	      {
			const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
			return this.http.get(this.config.apiBase + 'wc/v3/products/attributes/'+term+'/terms', { headers: myHeaders }).concatMap(data => {
				window.localStorage.setItem("attributestermsCacheTime", JSON.stringify(currentTime));/*set orders in localstorage*/
	        	window.localStorage.setItem('attributesterms'+term, JSON.stringify(data));
				return Observable.of(data);
			});
		  }
	}
	/*end*/
	/*product by filte*/
	public productsByfilter(adminToken: string, cterms: string, sterms: string, price: string, minprice: string, pageNo: string): Observable<Array<Product>> {
		const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
		return this.http.get<Array<Product>>(this.config.apiBase + 'wc/v3/products/' + '?attribute=color&attribute_term=' + cterms + '&attribute=size&attribute_term='+sterms+'&min_price='+minprice+'&max_price='+ price +'&per_page=20&page=' + pageNo + '&_embed', { headers: myHeaders }).concatMap(data => {
			return Observable.of(data);
		});
	}

	/* for order confirmation notification */
	public notification(adminToken: string, orderid: string) {
	const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': adminToken });
	return this.http
		.get<Array<Product>>(this.config.apiBase + 'custom-api/v1/notification/' + orderid, { headers: myHeaders })
		.concatMap(data => {
			return Observable.of(data);
		});
	}
	/*end*/

}