import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { AddressPage } from './address';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [AddressPage],
  imports: [
    IonicPageModule.forChild(AddressPage),
    IonicImageLoader
  ]
})
export class AddressPageModule {}
