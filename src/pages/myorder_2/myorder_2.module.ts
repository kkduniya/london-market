import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { Myorder_2Page } from './myorder_2';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [Myorder_2Page],
  imports: [
    IonicPageModule.forChild(Myorder_2Page),
    IonicImageLoader
  ]
})
export class Myorder_2PageModule {}
