import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { ShippiningPage } from './shippining';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [ShippiningPage],
  imports: [
    IonicPageModule.forChild(ShippiningPage),
    IonicImageLoader
  ]
})
export class ShippiningPageModule {}
