import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, NavParams, ModalController, Loading, LoadingController, ToastController } from 'ionic-angular';
import { Category } from "../../models/category.models";
import { Constants } from "../../models/constants.models";
import { Subscription } from '../../../node_modules/rxjs/Subscription';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Banner } from '../../models/banner.models';
/**
 * Generated class for the GiftingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-gifting',
  templateUrl: 'gifting.html',
  providers: [WordpressClient]
})
export class GiftingPage {
    private categoriesAllNew = new Array<Category>();
	private categoriesAll: any;
	private catsToShow: Array<Category>;
	private subscriptions: Array<Subscription> = [];
	private loading: Loading;
 	private loadingShown: Boolean = false;
 	private catid: any = '61';
 	private banners = new Array<Banner>();

  constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController, private service: WordpressClient,) {
  	this.presentLoading('Loading');
  	this.giftcategory();
  	this.loadBanners();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GiftingPage');
  }

  giftcategory()
  {
  	let subscription: Subscription = this.service.subcategoryofparent(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.catid)).
    subscribe(data => {
    	this.dismissLoading();
    	this.categoriesAll = data;
	}, err => {
		this.dismissLoading();
	});

	this.subscriptions.push(subscription);
  }

  shirtsPage(cat: Category) {
		if (cat.id != '-1') {
			this.navCtrl.push('ShirtsPage', { cat: cat });
		}
	}

  categories() {
	 this.navCtrl.push('CategoryPage');
	}

	loadBanners() {
	    let savedBanners: Array<Banner> = JSON.parse(window.localStorage.getItem('banners'));
	    if (savedBanners && savedBanners.length) {
	      this.banners = savedBanners;
	      let banners: Array<Banner> = this.banners;
	      let categories: Array<Category> = JSON.parse(window.localStorage.getItem(Constants.PRODUCT_CATEGORIES));
	      for (let ban of banners) {
	        for (let cat of categories) {
	          if (cat.id == ban.category) {
	            ban.catObj = cat;
	            break;
	          }
	        }
	      }
	    }
	  }

	private presentLoading(message: string) {
		this.loading = this.loadingCtrl.create({
			content: message
		});
		this.loading.onDidDismiss(() => { });
		this.loading.present();
		this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

	goBack() {
		this.navCtrl.push('HomePage');
	}
	
}
