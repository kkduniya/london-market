import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { GiftingPage } from './gifting';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [GiftingPage],
  imports: [
    IonicPageModule.forChild(GiftingPage),
    IonicImageLoader
  ]
})
export class GiftingPageModule {}
