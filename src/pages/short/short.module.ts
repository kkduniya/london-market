import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { ShortPage } from './short';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [ShortPage],
  imports: [
    IonicPageModule.forChild(ShortPage),
    IonicImageLoader
  ]
})
export class ShortPageModule {}
