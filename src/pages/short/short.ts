import { Component } from '@angular/core';
import { NavController, IonicPage,IonicPageModule, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-short',
  templateUrl: 'short.html'
})
export class ShortPage {
 
  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {

  }
  
       dismiss() {
    this.viewCtrl.dismiss();
  }

}
