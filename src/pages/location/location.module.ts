import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { LocationPage } from './location';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [LocationPage],
  imports: [
    IonicPageModule.forChild(LocationPage),
    IonicImageLoader
  ]
})
export class LocationPageModule {}
