import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { VerificationPage } from './verification';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [VerificationPage],
  imports: [
    IonicPageModule.forChild(VerificationPage),
    IonicImageLoader
  ]
})
export class VerificationPageModule {}
