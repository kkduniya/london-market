import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-verification ',
  templateUrl: 'verification.html'
})
export class VerificationPage {

  constructor(public navCtrl: NavController) {

  }
  
   loginPage(){
    this.navCtrl.setRoot('LoginPage');
    }

}
