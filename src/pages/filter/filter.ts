import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, ViewController } from 'ionic-angular';
import { Category } from "../../models/category.models";
import { Constants } from "../../models/constants.models";
import { Subscription } from '../../../node_modules/rxjs/Subscription';
import { WordpressClient } from '../../providers/wordpress-client.service';

@IonicPage()
@Component({
  selector: 'page-filter ',
  templateUrl: 'filter.html',
  providers: [WordpressClient]
})
export class FilterPage {
 private attributes = new Array<Category>();
 private terms: any;
 collection:any;
 colorfilter={white:false,purple:false,blue:false,brown:false,black:false,red:false,green:false,pink:false,yellow:false,multi:false};
 sizefilter={s:false,m:false,l:false,xl:false, xxl:false};
 price={one:false,three:false,five:false,ten:false,fifteen:false,thirty:false, lakhs:false};
 showcolor= new Array();
 showsize= new Array();
 showprice= new Array();
 colorvalue= new Array();
 sizevalue= new Array();
 pricevalue= new Array();
 minprice = new Array();
  constructor(public navCtrl: NavController, private service: WordpressClient, private viewCtrl: ViewController) {
 	/*this.all_attributes();*/
 	console.log(this.colorfilter);
  }
  
  dismiss() {
    this.viewCtrl.dismiss();
  }

   all_attributes() {
	let subscription: Subscription = this.service.get_attributes(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {
		console.log(data);
		this.attributes= data;
  	});
  }

  showterms(id){
  	let subscription: Subscription = this.service.get_attributes_term(window.localStorage.getItem(Constants.ADMIN_API_KEY), id).subscribe(data => {
		console.log(data);
		this.terms= data;
  	});
  }

  /*filters*/
  colortoggle(val, color)
  {
    var index = this.showcolor.findIndex(x => x == color);
    if(index===-1 && val==true)
    {
  		this.showcolor.push(color);
  	}
  	else{
  		let cid = this.showcolor.map(obj => obj).indexOf(color);
  		this.showcolor.splice(cid, 1);
  	}	
  }

  sizetoggle(val, size)
  {
    	var index = this.showsize.findIndex(x => x == size);
    	if(index===-1 && val==true)
    	{
  		this.showsize.push(size);
    	}
    	else{
    		let sid = this.showsize.map(obj => obj).indexOf(size);
    		this.showsize.splice(sid, 1);
    	}
    
  }

  pricetoggle(val, min, price)
  {
    	var index = this.showprice.findIndex(x => x == price);
    	if(index===-1 && val==true)
    	{
  		this.showprice.push(Number(price));
  	  }
    	else{
    		let pid = this.showprice.map(obj => obj).indexOf(price);
    		this.showprice.splice(pid, 1);
    	}

      var minindex = this.minprice.findIndex(x => x == min);
      if(minindex===-1 && val==true)
      {
      this.minprice.push(Number(min));
      }
      else{
        let mid = this.minprice.map(obj => obj).indexOf(min);
        this.minprice.splice(mid, 1);
      }
    console.log(this.minprice);
  }

  filters()
  {
    /*var obj = this.colorfilter;
    const identifiers = Object.keys(obj);
    this.colorvalue = identifiers.filter(function(key) {
      return obj[key];
    });

    var obj = this.sizefilter;
    const identifiers = Object.keys(obj);
    this.sizevalue = identifiers.filter(function(key) {
      return obj[key];
    });
  
    var obj = this.price;
    const identifiers = Object.keys(obj);
    this.pricevalue = identifiers.filter(function(key) {
      return obj[key];
    });*/
    console.log(this.showcolor,this.showsize,this.showprice);
    this.navCtrl.push('FilterproductPage', {color:this.showcolor, size: this.showsize, price:this.showprice, minprice:this.minprice});
  }

}
