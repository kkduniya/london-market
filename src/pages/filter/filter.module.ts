import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { FilterPage } from './filter';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [FilterPage],
  imports: [
    IonicPageModule.forChild(FilterPage),
    IonicImageLoader
  ]
})
export class FilterPageModule {}
