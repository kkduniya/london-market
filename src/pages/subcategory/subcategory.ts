import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, NavParams } from 'ionic-angular';
import { Category } from "../../models/category.models";
import { Constants } from "../../models/constants.models";

/**
 * Generated class for the SubcategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-subcategory',
  templateUrl: 'subcategory.html',
})
export class SubcategoryPage {
private category: Category;
private catsToShow: Array<Category>;
private catp: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.catsToShow = navParams.get('cat');
    this.catp = navParams.get('parent');
    console.log(this.catp);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubcategoryPage');
  }

  shirtsPage(cat: Category) {
		if (cat.id != '-1') {
			this.navCtrl.push('ShirtsPage', { cat: cat });
		}
	}

   Category_Page(cats) {
    let categories: Array<Category> = JSON.parse(window.localStorage.getItem(Constants.PRODUCT_CATEGORIES));
      let parentWithChild = new Array<Category>();
          for (let cat of categories) {
            if (cat.parent == cats.id) {
              parentWithChild.push(cat);
            }
          }
    if (parentWithChild && parentWithChild.length>0){
      this.navCtrl.push('SubsubcatPage', { cat: parentWithChild });
    }
    else{
      this.navCtrl.push('ShirtsPage', { cat: cats });
    }
  }

  searchPage() {
      this.navCtrl.push('SearchPage');
      // let modal = this.modalCtrl.create(SearchPage);
      // modal.present();
    }

}
