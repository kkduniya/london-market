import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { SubcategoryPage } from './subcategory';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [SubcategoryPage],
  imports: [
    IonicPageModule.forChild(SubcategoryPage),
    IonicImageLoader
  ]
})
export class SubcategoryPageModule {}
