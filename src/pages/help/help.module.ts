import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { HelpPage } from './help';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [HelpPage],
  imports: [
    IonicPageModule.forChild(HelpPage),
    IonicImageLoader
  ]
})
export class HelpPageModule {}
