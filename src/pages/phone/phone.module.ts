import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { PhonePage } from './phone';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [PhonePage],
  imports: [
    IonicPageModule.forChild(PhonePage),
    IonicImageLoader
  ]
})
export class PhonePageModule {}
