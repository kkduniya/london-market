import { Component, Inject } from '@angular/core';
import { Events, IonicPage,IonicPageModule, App, Platform, NavController, NavParams, AlertController, LoadingController, ToastController, ViewController } from 'ionic-angular';
import firebase from 'firebase';
import { Firebase } from '@ionic-native/firebase/ngx';
import { UserResponse } from "../../models/user-response.models";
import { Constants } from "../../models/constants.models";
import { AuthCredential } from "../../models/auth-credential.models";
import { AuthResponse } from "../../models/auth-response.models";
import { Address } from '../../models/address.models';
import { RegisterRequest } from "../../models/register-request.models";
import { RegisterResponse } from "../../models/register-response.models";

import { WordpressClient } from '../../providers/wordpress-client.service';
import { Subscription } from "rxjs/Subscription";
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Helper } from '../../models/helper.models';
/**
 * Generated class for the PhonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

/*@IonicPage()*/
@IonicPage()
@Component({
  selector: 'page-phone',
  templateUrl: 'phone.html',
  providers: [Firebase, WordpressClient]
})

export class PhonePage {
  public recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  loading: any;
  loadingShown: boolean = false;
  captchanotvarified: boolean = true;
  result: any;
  verfificationId: any;
  otpNotsent: boolean = false;
  countries: any;
  countryCode: string = '';
  private registerRequest: RegisterRequest;
  private subscriptions: Array<Subscription> = [];
  private registerResponse: RegisterResponse;

  constructor(@Inject(APP_CONFIG) public config: AppConfig, public navCtrl: NavController, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public view: ViewController, private firebase: Firebase, public platform: Platform,
    public events: Events, public service: WordpressClient, navParam: NavParams) {
    this.registerRequest = navParam.get("registerRequest");
    this.getCountries();
  }

  getCountries() {
    this.service.getCountries().subscribe(data => {
      console.log("Countries fetched");
      this.countries = data;
      // console.log(data);
    }, err => {
      console.log(err);
    })
  }

  createUser() {
    if (!this.countryCode || !this.countryCode.length) {
      this.showToast("Select country");
      return;
    }
    if (!this.registerRequest.username || !this.registerRequest.username.length) {
      this.showToast("Enter number");
      return;
    }
    let alert = this.alertCtrl.create({
      title: "+" + this.countryCode + this.registerRequest.username,
      message: "Continue using this phone number for OTP verification?",
      buttons: [{
        text: "No",
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: "Yes",
        handler: () => {
          this.presentLoading("Checking mobile no.");
          this.registerRequest.password = Math.random().toString(36).slice(-6);
          this.registerRequest.username = this.countryCode + this.registerRequest.username;
          let subscription: Subscription = this.service.createUser(window.localStorage.getItem(Constants.ADMIN_API_KEY), this.registerRequest).subscribe(data => {
            this.dismissLoading();
            this.registerResponse = data;
            this.navCtrl.setRoot('OtpPage', { registerRequest: this.registerRequest, userId: data.id });
            //user not found now we can send the sms on this number
          }, err => {
            console.log(err);
            this.dismissLoading();
            let userId = Helper.extractUserIdFromError(err);
            if (userId == -1) {
              this.showToast("Mobile no. already registered");
            } else {
              this.navCtrl.setRoot('OtpPage', { registerRequest: this.registerRequest, userId: userId });
            }
          });
          this.subscriptions.push(subscription);
        }
      }]
    });
    alert.present();
  }

  makeExitAlert() {
    const alert = this.alertCtrl.create({
      title: 'App termination',
      message: 'Do you want to close the app?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Application exit prevented!');
        }
      }, {
        text: 'Close App',
        handler: () => {
          this.platform.exitApp(); // Close this application
        }
      }]
    });
    alert.present();
  }

  private presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });

    this.loading.onDidDismiss(() => { });

    this.loading.present();
    this.loadingShown = true;
  }

  private dismissLoading() {
    if (this.loadingShown) {
      this.loadingShown = false;
      this.loading.dismiss();
    }
  }

  private showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
}