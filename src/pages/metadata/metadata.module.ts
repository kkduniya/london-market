import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { MetadataPage } from './metadata';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [MetadataPage],
  imports: [
    IonicPageModule.forChild(MetadataPage),
    IonicImageLoader
  ]
})
export class MetadataPageModule {}
