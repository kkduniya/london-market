import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, NavParams, ModalController, ToastController } from 'ionic-angular';
import { UserResponse } from "../../models/user-response.models";
import { Constants } from "../../models/constants.models";
import { Global } from '../../providers/global';
import { Subscription } from 'rxjs/Subscription';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [WordpressClient, Global]
})
export class ProfilePage {
  private user: any;
  private cartTotal = 0;
  private userimage:any;
  private profile: any;
  private subscriptions: Array<Subscription> = [];
  constructor(private toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams, private global: Global, public modalCtrl: ModalController, private service: WordpressClient,private camera: Camera){
  	this.user = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));
    console.log(this.user);
      if(this.user && this.user.meta_data!=null && this.user.meta_data!=undefined)
      {   let profilemeta = this.user.meta_data;
          if(this.user.meta_data.length > 0)
          {
            for(let i=0; i<this.user.meta_data.length; i++)
            {
              if(profilemeta[i].key=='pimage')
              {
                this.profile = profilemeta[i].value;
              }
            }
          }
      }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.cartTotal = Number(this.global.getCartItemsCount());
  }

  cartPage() {
    //this.navCtrl.push(CartPage);
    let modal = this.modalCtrl.create('CartPage');
    modal.onDidDismiss(() => {
      this.cartTotal = Number(this.global.getCartItemsCount());
    });
    modal.present();
  }

  myorders(){
  	this.navCtrl.push('Myorder_2Page');
  }

  myaccount(){
  	this.navCtrl.push('My_accountPage');
  }
  
  search() {
    this.navCtrl.push('SearchPage');
    // let modal = this.modalCtrl.create(SearchPage);
    // modal.present();
  }

  profileimage() {
     const options: CameraOptions = {
      quality : 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType : 1,
      allowEdit : true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 100,
      targetHeight: 100,
      saveToPhotoAlbum: false
    };

    this.camera.getPicture(options).then((imageData) => {
      let baseImage = 'data:image/jpeg;base64,' + imageData;
      this.userimage = baseImage;
      this.update_data(baseImage);
      this.showToast('Updated Profile Photo');
      console.log(this.user);
    }, (err) => {
      // Handle error
    });

  }

  update_data(userimage){
      let subscription: Subscription = this.service.updateUser(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.user.id), { "meta_data": [{ "key": "pimage","value": userimage }] } ).subscribe(data => {
        localStorage.setItem('UserKey', JSON.stringify(data));
        this.profile = userimage;
      });
      this.subscriptions.push(subscription);
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }


}
