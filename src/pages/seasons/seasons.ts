import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule,NavParams } from 'ionic-angular';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Subscription } from "rxjs/Subscription";
import { Constants } from "../../models/constants.models";
import { UserResponse } from "../../models/user-response.models";

/**
 * Generated class for the SeasonsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-seasons',
  templateUrl: 'seasons.html',
  providers: [WordpressClient]
})
export class SeasonsPage {
  private subscriptions: Array<Subscription> = [];
  private seasons= [];

  constructor(public navCtrl: NavController, public navParams: NavParams ,private service: WordpressClient,) {
    
    let subscription: Subscription = this.service.productstag(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {
          /* filter seasons*/

          for(var i=0; i<data.length; i++)
          {  
            if(data[i].id =='283' || data[i].id == '284' || data[i].id =='285' || 
              data[i].id == '286' || data[i].id == '287' || data[i].id == '288' || 
              data[i].id =='289' || data[i].id == '290' || data[i].id == '291' 
              || data[i].id == '292' || data[i].id == '293')
            {
              this.seasons.push(data[i]);
            }

          }
        /*end*/
          this.seasons.sort(function(a, b) {
              return (a.id - b.id);
          }).sort(function(a, b) {
              return (a.name - b.name);
          });
          console.log(this.seasons);
    });


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SeasonsPage');
  }

  tagspage(id) {
     console.log(id);
      this.navCtrl.push('TagproductPage',{tag_id:id});
  }

}
