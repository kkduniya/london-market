import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { SeasonsPage } from './seasons';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [SeasonsPage],
  imports: [
    IonicPageModule.forChild(SeasonsPage),
    IonicImageLoader
  ]
})
export class SeasonsPageModule {}
