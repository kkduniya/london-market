import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { TermPage } from './term';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [TermPage],
  imports: [
    IonicPageModule.forChild(TermPage),
    IonicImageLoader
  ]
})
export class TermPageModule {}
