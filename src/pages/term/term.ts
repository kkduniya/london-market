import { Component } from '@angular/core';
import { NavController, IonicPage,IonicPageModule, NavParams, ViewController, ModalController } from 'ionic-angular';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Global } from '../../providers/global';
import { Subscription } from "rxjs/Subscription";
import { Constants } from "../../models/constants.models";

/**
 * Generated class for the TermPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-term',
  templateUrl: 'term.html',
  providers: [WordpressClient, Global]
})
export class TermPage {
private pageid = 910;
private pagedata:any;
private subscriptions: Array<Subscription> = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private service: WordpressClient ,private global: Global) {
  	
  	this.pagedata = this.navParams.get('pagedata');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermPage');
  }

  dismiss() {
		this.viewCtrl.dismiss();
	}

}
