import { Component, Inject } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, NavParams, AlertController, Loading, LoadingController, ToastController, App } from 'ionic-angular';
import { PaymentGateway } from "../../models/payment-gateway.models";
import { Constants } from "../../models/constants.models";

import { WordpressClient } from '../../providers/wordpress-client.service';
import { Global } from '../../providers/global';
import { Subscription } from "rxjs/Subscription";
import { CartItem } from "../../models/cart-item.models";
import { OrderRequest } from "../../models/order-request.models";
import { Address } from "../../models/address.models";
import { ShippingLine } from "../../models/shipping-line.models";
import { UserResponse } from "../../models/user-response.models";
import { OrderResponse } from "../../models/order-response.models";
import { Currency } from "../../models/currency.models";
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { sha512 } from 'js-sha512';
import { APP_CONFIG, AppConfig } from '../../app/app.config';
import { OrderUpdateRequest } from '../../models/order-update-request.models';
import { Coupon } from '../../models/coupon.models';
import { Helper } from '../../models/helper.models';
import { ShippingMethod } from '../../models/shipping-method.models';

@IonicPage()
@Component({
	selector: 'page-payment ',
	templateUrl: 'payment.html',
	providers: [WordpressClient]
})

export class PaymentPage {
	private loading: Loading;
	private loadingShown: Boolean = false;
	private placedPagePushed: Boolean = false;
	private paymentDone: Boolean = false;
	private paymentFailAlerted: Boolean = false;

	private subscriptions: Array<Subscription> = [];
	private paymentGateways = new Array<PaymentGateway>();
	private cartItems: Array<CartItem>;
	private selectedPaymentGateway;
	private selectedAddress: Address;
	private orderRequest: OrderRequest;
	private orderResponse: OrderResponse;
	private orderId = -1;
	private user: UserResponse;
	private totalItems = 0;
	private total = 0;
	private couponApplied = false;
	private pickupTime = 0;
	private deliveryTime = 0;
	private shippingChargeGlobal: number;

	constructor(@Inject(APP_CONFIG) private config: AppConfig, private iab: InAppBrowser, private toastCtrl: ToastController, public navCtrl: NavController, private navParams: NavParams, private service: WordpressClient, private loadingCtrl: LoadingController, private alertCtrl: AlertController, public appCtrl: App) {
		this.cartItems = this.navParams.get('cart');
		this.totalItems = this.navParams.get('totalItems');
		this.total = this.navParams.get('total');
		this.shippingChargeGlobal = this.navParams.get('shippingChargeGlobal');

		let paymentGateways = JSON.parse(window.localStorage.getItem(Constants.PAYMENT_GATEWAYS));
		this.selectedAddress = JSON.parse(window.localStorage.getItem(Constants.SELECTED_ADDRESS));

		if (paymentGateways != null) {
			for (let pg of paymentGateways) {
				if (pg.enabled && this.paymentImplemented(pg.id)) {
					this.paymentGateways.push(pg);
				}
			}
		}
	}

	ionViewWillLeave() {
		this.subscriptions.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
		this.dismissLoading();
	}

	paymentImplemented(id) {
		return id === "razorpay" || id === "wpl_paylabs_payu" || id === "cod";
	}

	paymentMethod(paymentGateway) {
		this.selectedPaymentGateway = paymentGateway;
	}

	placedPage() {
		if (this.selectedPaymentGateway == null) {
				this.showToast('Choose payment method.');
		} else {
			this.orderRequest = new OrderRequest();
			this.orderRequest.payment_method = this.selectedPaymentGateway.id ? this.selectedPaymentGateway.id : "cod";
			this.orderRequest.payment_method_title = this.selectedPaymentGateway.title ? this.selectedPaymentGateway.title : "cod";
			this.orderRequest.set_paid = false;
			this.orderRequest.billing = this.selectedAddress;
			this.orderRequest.shipping = this.selectedAddress;
			this.user = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));
			this.orderRequest.customer_id = String(this.user.id);

			let selectedShippingMethod: ShippingMethod = JSON.parse(window.localStorage.getItem(Constants.SELECTED_SHIPPING_METHOD));
			if (selectedShippingMethod) {
				let shippingTotal = 0;
				for (let ci of this.cartItems) {
					if (!ci.product.shipping_cost_use_global && ci.product.shipping_cost != 1)
						shippingTotal = shippingTotal + ci.product.shipping_cost;
				}
				if (this.shippingChargeGlobal != -1) {
					shippingTotal = shippingTotal + this.shippingChargeGlobal;
				}
				this.orderRequest.shipping_lines = new Array<ShippingLine>();
				this.orderRequest.shipping_lines.push(new ShippingLine(selectedShippingMethod.method_id, selectedShippingMethod.method_title, String(shippingTotal)));
			}

			this.orderRequest.line_items = this.cartItems;
			for (let item of this.orderRequest.line_items) {
				item.product = null;
			}

			this.presentLoading('Creating order');
			let coupon: Coupon = JSON.parse(window.localStorage.getItem(Constants.SELECTED_COUPON));
			let subscription: Subscription = this.service.createOrder(window.localStorage.getItem(Constants.ADMIN_API_KEY), this.orderRequest).subscribe(data => {
				this.orderResponse = data;
				this.orderId = data.id;
				if (coupon) {
					this.applyCoupon(coupon);
				} else {
					this.orderPlaced();
				}
			}, err => {
				console.log(err);
				this.dismissLoading();
				let orderId = Helper.extractOrderIdFromError(err);
				if (orderId != -1) {
					this.orderId = orderId;
					if (coupon) {
						this.applyCoupon(coupon);
					} else {
						this.orderPlaced();
					}
				} else {
						this.showToast('Unable to place order');
					this.appCtrl.getRootNav().setRoot('HomePage');
				}
			});
			this.subscriptions.push(subscription);
		}
	}

	applyCoupon(coupon) {
		let couponSubs: Subscription = this.service.applyCouponCode(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.orderId), coupon.code).subscribe(data => {
			this.couponApplied = true;
			window.localStorage.removeItem(Constants.SELECTED_COUPON);
				this.showToast('Coupon applied');
			this.orderPlaced();
		}, err => {
			console.log(err);
			this.dismissLoading();
		});
		this.subscriptions.push(couponSubs);
	}

	orderPlaced() {
		this.dismissLoading();
		if (this.selectedPaymentGateway.id && this.selectedPaymentGateway.id === "cod") {
			this.clearCart();
			this.paymentSuccessCOD();
			/*this.navCtrl.setRoot('PlacedPage');*/
		} else if (this.selectedPaymentGateway.id && this.selectedPaymentGateway.id === "razorpay") {
			this.initrazorpay();
		} else {
			this.showToast('Processed via Cash on delivery');
			this.clearCart();
			this.navCtrl.setRoot('PlacedPage');
		}
	}

	/*razor pay*/
	initrazorpay() {
		let name = this.user.first_name && this.user.first_name.length ? this.user.first_name : this.user.username;
		let mobile = this.user.username;
		let email = this.user.email;
		let bookingId = String(this.orderResponse.id);
		let key = this.config.razorpaykey;
		let amt = this.total * 100;
		
		var options = {
	      description: 'London Market',
	      image: 'https://londonmarket.in/wp-content/uploads/2020/06/logo.jpg',
	      currency: 'INR',
	      key: key,
	      amount: amt,
	      name: name,
	      prefill: {
	        email: email,
	        contact: mobile,
	        name: name
	      },
	      theme: {
	        color: '#893652'
	      },
	      modal: {
	        ondismiss: function() {
	          alert('dismissed')
	        }
	      }
    	};

	    var successCallback = (payment_id) => { 
		  //alert('payment_id: ' + payment_id);
		  this.paymentSuccess(payment_id);
		};

		var cancelCallback = (error) => { 
		  //alert(error.description + ' (Error ' + error.code + ')');
		  this.paymentFailure();
		};

    	RazorpayCheckout.open(options, successCallback, cancelCallback);
	}
	/*end*/

	paymentFailure() {
		this.paymentFailAlerted = true;
		let subscription: Subscription = this.service.updateOrder(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.orderResponse.id), new OrderUpdateRequest('cancelled')).subscribe(data => {
		}, err => {
			console.log(err);
		});
		this.subscriptions.push(subscription);
			let alert = this.alertCtrl.create({
				title: 'Payment failure',
				message: 'Unfortunately payment has failed hence order has been cancelled. Item(s) still exists in your cart, you can retry later.',
				buttons: [{
					text: 'Okay',
					role: 'cancel',
					handler: () => {
						this.done();
						console.log('Okay clicked');
					}
				}]
			});
			alert.present();
	}

	paymentSuccess(payment_id) {
		this.paymentDone = true;
		this.clearCart();
			this.presentLoading('Just a moment');
		let subscription: Subscription = this.service.updateOrder(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.orderResponse.id), {status: 'processing', set_paid: true, transaction_id: payment_id }).subscribe(data => {
			/*new kk code for notification*/
			 this.service.notification(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.orderResponse.id)).subscribe(data => {	});	
			//set coupon code applied

			let applied = JSON.parse(window.localStorage.getItem(Constants.SELECTED_COUPON));

			if(applied)
			{
				window.localStorage.setItem(applied.code.toLowerCase(),applied.code.toUpperCase());
			}
			/*end*/
			this.navCtrl.setRoot('PlacedPage');
			this.done();
		}, err => {
			this.done();
			//this.paymentSuccess();
			console.log(err);
		});
		this.subscriptions.push(subscription);
	}

	/*initPayUMoney() {
		let name = this.user.first_name && this.user.first_name.length ? this.user.first_name : this.user.username;
		let mobile = this.user.username;
		let email = this.user.email;
		let bookingId = String(Math.floor(Math.random() * (99 - 10 + 1) + 10)) + this.orderId;
		let productinfo = this.orderId;
		let salt = this.config.payuSalt;
		let key = this.config.payuKey;
		let amt = this.couponApplied ? this.total : this.totalItems;
		let string = key + '|' + bookingId + '|' + amt + '|' + productinfo + '|' + name + '|' + email + '|||||||||||' + salt;
		let encrypttext = sha512(string);

		//let url = "payumoney/payuBiz.html?amt=" + amt + "&name=" + name + "&mobileNo=" + mobile + "&email=" + email + "&bookingId=" + bookingId + "&productinfo=" + productinfo + "&salt=" + salt + "&key=" + key;
		let url = "payumoney/payuBiz.html?amt=" + amt + "&name=" + name + "&mobileNo=" + mobile + "&email=" + email + "&bookingId=" + bookingId + "&productinfo=" + productinfo + "&hash=" + encrypttext + "&salt=" + salt + "&key=" + key;
		
		let options: InAppBrowserOptions = {
			location: 'yes',
			clearcache: 'yes',
			zoom: 'yes',
			toolbar: 'no',
			closebuttoncaption: 'back'
		};
		const browser: any = this.iab.create(url, '_blank', options);
		browser.on('loadstop').subscribe(event => {
			browser.executeScript({
				file: "payumoney/payumoneyPaymentGateway.js"
			});

			if (event.url == "http://localhost/success.php") {
				this.paymentSuccess();
				browser.close();
			}
			if (event.url == "http://localhost/failure.php") {
				this.paymentFailure();
				browser.close();
			}
		});
		browser.on('exit').subscribe(event => {
			if (!this.paymentDone && !this.paymentFailAlerted) {
				this.paymentFailure();
			}
		});
		browser.on('loaderror').subscribe(event => {
			this.showToast('something_went_wrong');
		});
	}

	paymentFailure() {
		this.paymentFailAlerted = true;
		let subscription: Subscription = this.service.updateOrder(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.orderId), new OrderUpdateRequest('cancelled')).subscribe(data => {
		}, err => {
			console.log(err);
		});
		this.subscriptions.push(subscription);


			let alert = this.alertCtrl.create({
				title: 'Payment failure',
				message: 'Unfortunately payment has failed hence order has been cancelled. Item(s) still exists in your cart, you can retry later.',
				buttons: [{
					text: 'ok',
					role: 'cancel',
					handler: () => {
						this.done();
						console.log('Okay clicked');
					}
				}]
			});
			alert.present();
	}

	paymentSuccess() {
		this.paymentDone = true;
		this.clearCart();
			this.presentLoading('Just a moment');
		let subscription: Subscription = this.service.updateOrder(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.orderId), { set_paid: true }).subscribe(data => {
			this.done();
		}, err => {
			this.done();
			this.paymentSuccess();
			console.log(err);
		});
		this.subscriptions.push(subscription);
	}*/

	paymentSuccessCOD() {
		this.paymentDone = true;
		this.clearCart();
			this.presentLoading('Just a moment');
		let subscription: Subscription = this.service.updateOrder(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.orderResponse.id), { status: 'processing', set_paid: true }).subscribe(data => {
			/*new kk code for notification*/
			 this.service.notification(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.orderResponse.id)).subscribe(data => {	
			 	console.log('send notification');
			 });	
			//set coupon code applied
			let applied = JSON.parse(window.localStorage.getItem(Constants.SELECTED_COUPON));
			if(applied)
			{
				window.localStorage.setItem(applied.code.toLowerCase(),applied.code.toUpperCase());
			}
			/*end*/
			this.clearCart();
			/*this.navCtrl.setRoot('PlacedPage');*/
			this.done();
		}, err => {
			this.done();
			//this.paymentSuccess();
			console.log(err);
		});
		this.subscriptions.push(subscription);
	}

	done() {
		if (!this.placedPagePushed) {
			this.placedPagePushed = true;
			this.dismissLoading();
			this.appCtrl.getRootNav().setRoot(this.paymentFailAlerted ? 'HomePage' : 'PlacedPage');
		}
	}

	private presentLoading(message: string) {
		this.loading = this.loadingCtrl.create({
			content: message
		});

		this.loading.onDidDismiss(() => { });

		this.loading.present();
		this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

	private presentErrorAlert(msg: string) {
		let alert = this.alertCtrl.create({
			title: 'Error',
			subTitle: msg,
			buttons: ['Dismiss']
		});
		alert.present();
	}

	showToast(message: string) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 3000,
			position: 'bottom'
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});
		toast.present();
	}

	clearCart() {
		let cartItems = new Array<CartItem>();
		window.localStorage.setItem('cartItems', JSON.stringify(cartItems));
	}

	goBack() {
		this.navCtrl.pop();
	}
	
}