import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { PaymentPage } from './payment';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [PaymentPage],
  imports: [
    IonicPageModule.forChild(PaymentPage),
    IonicImageLoader
  ]
})
export class PaymentPageModule {}
