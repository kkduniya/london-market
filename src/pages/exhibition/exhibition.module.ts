import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { ExhibitionPage } from './exhibition';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [ExhibitionPage],
  imports: [
    IonicPageModule.forChild(ExhibitionPage),
    IonicImageLoader
  ]
})
export class ExhibitionPageModule {}
