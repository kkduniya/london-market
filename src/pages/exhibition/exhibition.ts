import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, NavParams, ViewController, ToastController, App, Loading, LoadingController } from 'ionic-angular';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Subscription } from "rxjs/Subscription";
import { Constants } from "../../models/constants.models";
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

/**
 * Generated class for the ExhibitionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-exhibition',
  templateUrl: 'exhibition.html',
  providers: [WordpressClient]
})
export class ExhibitionPage {
private policy:any;
private pageid = 1773;
private loading: Loading;
private loadingShown: Boolean = false;
private pagedata:any;
private subscriptions: Array<Subscription> = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private service: WordpressClient, private loadingCtrl: LoadingController, private iab: InAppBrowser) {
  	  this.presentLoading('Loading...');
      this.get_exhibitions();
  }

  get_exhibitions()
  {
    this.service.exhibitionsdata(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {
        this.dismissLoading();
        this.pagedata = data;
         console.log(this.pagedata);
      }, err => {
        this.pagedata = err;
        console.log(err);
      });
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExhibitionPage');
  }

/**/

  exhibition(url)
  {
    console.log(url);
    let options : InAppBrowserOptions = {
    location : 'no',//Or 'no'
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only
    toolbar : 'no', //iOS only
    enableViewportScale : 'no', //iOS only
    allowInlineMediaPlayback : 'no',//iOS only
    presentationstyle : 'pagesheet',//iOS only
    fullscreen : 'yes'//Windows only
    };
    this.iab.create(url, '_system', options);
  }


  private presentLoading(message: string) {
		this.loading = this.loadingCtrl.create({
			content: message
		});
		this.loading.onDidDismiss(() => { });
		this.loading.present();
		this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

  goBack() {
    this.navCtrl.setRoot('HomePage');
  }

}
