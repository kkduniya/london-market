import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { My_accountPage } from './my_account';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [My_accountPage],
  imports: [
    IonicPageModule.forChild(My_accountPage),
    IonicImageLoader
  ]
})
export class My_accountPageModule {}
