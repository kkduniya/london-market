import { Component } from '@angular/core';
import { Nav, NavController,IonicPage,IonicPageModule, ModalController, ToastController, AlertController } from 'ionic-angular';
import { Address } from "../../models/address.models";
import { Constants } from "../../models/constants.models";
import { UserResponse } from "../../models/user-response.models";
import { Subscription } from 'rxjs/Subscription';
import { WordpressClient } from '../../providers/wordpress-client.service';


@IonicPage()
@Component({
	selector: 'page-my_account ',
	templateUrl: 'my_account.html',
	providers: [WordpressClient]
})
export class My_accountPage {
	account: string = "profile";
	private user: UserResponse;
	private selectedAddress: Address;
	private addressChangeText = 'Change';
	private subscriptions: Array<Subscription> = [];
	private pageid = 910;
	private pagedata:any;

	constructor(public navCtrl: NavController, private toastCtrl: ToastController, private service: WordpressClient, public modalCtrl: ModalController, private alertCtrl: AlertController) {
		this.user = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));
	}

	ionViewDidEnter() {
		this.selectedAddress = JSON.parse(window.localStorage.getItem(Constants.SELECTED_ADDRESS));
		if(this.selectedAddress == null)
		{
			this.addressChangeText = 'Add';
		}
		else{
			this.addressChangeText = 'Change';
		}
		/*this.translate.get(this.selectedAddress == null ? 'add' : 'change').subscribe(value => {
			this.addressChangeText = value;
		});*/
	}

	updateInfo() {
		if (!this.user.first_name || !this.user.first_name.length) {
				this.showToast('Enter first name');
		} else if (!this.user.last_name || !this.user.last_name.length) {
				this.showToast('Enter last name');
		} else {
				this.showToast('Updated');
			window.localStorage.setItem(Constants.USER_KEY, JSON.stringify(this.user));
			let subscription: Subscription = this.service.updateUser(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.user.id), { first_name: this.user.first_name, last_name: this.user.last_name }).subscribe(data => {
			}, err => {
			});
			this.subscriptions.push(subscription);
		}
	}

	term()
	{
		let subscription: Subscription = this.service.getPage(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.pageid)).
	    subscribe(data => {
	      this.pagedata = data;
	    }, err => {
	      this.pagedata = err;
	    });
	    this.subscriptions.push(subscription);

	    let termModal = this.modalCtrl.create('TermPage', {pagedata:this.pagedata});
   		termModal.present();
	}

	showToast(message: string) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 3000,
			position: 'bottom'
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});
		toast.present();
	}

	addressPage() {
		this.navCtrl.push('AddressSelectPage', { action: 'choose' });
	}

	isReadonly() {
		return true;
	}

	searchPage() {
		let modal = this.modalCtrl.create('SearchPage');
		modal.present();
	}


	cartPage() {
		let modal = this.modalCtrl.create('CartPage');
		modal.present();
	}

	call(phoneNumber)
	{
	  	window.open('tel:' + phoneNumber, '_system');
	}

    openemail(email){
  		window.open('mailto:' + email, '_system');
    }

}
