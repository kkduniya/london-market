import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { VendorsproductPage } from './vendorsproduct';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [VendorsproductPage],
  imports: [
    IonicPageModule.forChild(VendorsproductPage),
    IonicImageLoader
  ]
})
export class VendorsproductPageModule {}
