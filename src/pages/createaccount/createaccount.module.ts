import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { CreateaccountPage } from './createaccount';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [CreateaccountPage],
  imports: [
    IonicPageModule.forChild(CreateaccountPage),
    IonicImageLoader
  ]
})
export class CreateaccountPageModule {}
