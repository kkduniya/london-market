import { Component, Inject } from '@angular/core';
import { NavController, IonicPage,IonicPageModule, AlertController, Loading, LoadingController, ToastController, Events, ModalController } from 'ionic-angular';

import { WordpressClient } from '../../providers/wordpress-client.service';
import { Subscription } from "rxjs/Subscription";
import { AuthResponse } from "../../models/auth-response.models";
import { RegisterRequest } from "../../models/register-request.models";
import { RegisterResponse } from "../../models/register-response.models";
import { UserResponse } from "../../models/user-response.models";
import { Constants } from "../../models/constants.models";
import { AuthCredential } from "../../models/auth-credential.models";
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Helper } from '../../models/helper.models';

@IonicPage()
@Component({
	selector: 'page-createaccount',
	templateUrl: 'createaccount.html',
	providers: [WordpressClient]
})

export class CreateaccountPage {
	private loading: Loading;
	private loadingShown: Boolean = false;
	private authError = "";
	private subscriptions: Array<Subscription> = [];
	// private registerRequest: RegisterRequest = new RegisterRequest('prpwebs@gmail.com', '33333331', '123456');
	private registerRequest: RegisterRequest = new RegisterRequest('', '', '', '', '');
	private registerRequestPasswordConfirm: string = '';
	private registerResponse: RegisterResponse;
	buttonDisabled: Boolean = true;
	countries: any;
	countryCode: string = '';

	constructor(@Inject(APP_CONFIG) private config: AppConfig, private events: Events, private toastCtrl: ToastController,
		public navCtrl: NavController, private service: WordpressClient,
		private loadingCtrl: LoadingController, private alertCtrl: AlertController,
		public modalCtrl: ModalController) {

	}

	ionViewDidLoad() {
		this.getCountries();
	}

	getCountries() {
		this.service.getCountries().subscribe(data => {
			console.log("Countries fetched");
			this.countries = data;
			// console.log(data);
		}, err => {
			console.log(err);
		})
	}

	register() {
		this.authError = "";
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if (this.registerRequest.first_name == "" || !this.registerRequest.first_name.length) {
				this.showToast('Enter valid username');
		} else if (this.registerRequest.last_name == "" || !this.registerRequest.last_name.length) {
				this.showToast('Enter valid username');
		} else if (!this.countryCode || !this.countryCode.length) {
			this.showToast('Choose country code');
		} else if (!this.registerRequest.username.length || this.registerRequest.username.length!=10) {
			console.log(this.registerRequest.username.length);
				this.showToast('Enter valid phone number');
		} else if (this.registerRequest.email.length <= 5 || !reg.test(this.registerRequest.email)) {
				this.showToast('Enter valid email address');
		} else if (this.registerRequest.password.length == 0 || !(this.registerRequest.password === this.registerRequestPasswordConfirm)) {
				this.showToast('Enter valid passwords, twice.');
		} else {
				
			//this.registerRequest.username = this.registerRequest.username;
			this.navCtrl.setRoot('OtpPage', { registerRequest: this.registerRequest, dialcode:this.countryCode });
			
		}
	}

	private signIn(userId: string, username: string, password: string) {
		let credentials: AuthCredential = new AuthCredential(username, password);
		let subscription: Subscription = this.service.getAuthToken(credentials)
			.subscribe(data => {
				let authResponse: AuthResponse = data;
				window.localStorage.setItem(Constants.USER_API_KEY, authResponse.token);
				this.getUser(userId);
			}, err => {
				this.dismissLoading();
				this.presentErrorAlert('Error', 'Unable to login with provided credentials');
			});
		this.subscriptions.push(subscription);
	}

	private getUser(userId: string) {
		let subscription: Subscription = this.service.getUser(window.localStorage.getItem(Constants.ADMIN_API_KEY), userId).subscribe(data => {
			this.dismissLoading();
			let userResponse: UserResponse = data;
			window.localStorage.setItem(Constants.USER_KEY, JSON.stringify(userResponse));
			this.navCtrl.setRoot('HomePage');
			this.events.publish('user:login');
		}, err => {
			this.dismissLoading();
			this.presentErrorAlert('Error', 'Unable to login with provided credentials');
		});
		this.subscriptions.push(subscription);
	}

	signinPage() {
		this.navCtrl.setRoot('LoginPage');
	}

	private presentLoading(message: string) {
		this.loading = this.loadingCtrl.create({
			content: message
		});

		this.loading.onDidDismiss(() => { });

		this.loading.present();
		this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

	private presentErrorAlert(title: string, msg: string) {
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: msg,
			buttons: ['Dismiss']
		});
		alert.present();
	}

	showToast(message: string) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 3000,
			position: 'bottom'
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});
		toast.present();
	}
}
