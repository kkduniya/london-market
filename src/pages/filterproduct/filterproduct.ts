import { Component } from '@angular/core';
import { NavController, IonicPage,IonicPageModule,AlertController, Loading, LoadingController, ToastController, MenuController, ModalController, NavParams, ActionSheetController  } from 'ionic-angular';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Global } from '../../providers/global';
import { Subscription } from "rxjs/Subscription";
import { Category } from "../../models/category.models";
import { Constants } from "../../models/constants.models";
import { Product } from "../../models/product.models";
import { Currency } from "../../models/currency.models";

/**
 * Generated class for the FilterproductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filterproduct',
  templateUrl: 'filterproduct.html',
  providers: [WordpressClient, Global]
})
export class FilterproductPage {
    private category: Category;
	private loading: Loading;
	private loadingShown: Boolean = false;
	private subscriptions: Array<Subscription> = [];
	private productsAll = new Array<Array<Product>>();
	private productsResponse = new Array<Product>();
	private productsAllPage: number = 1;
	private currencyIcon: string;
	private currencyText: string;
	private cartTotal = 0;
	private wishlistproducts= [];
	private wishcount:any;
	private wishcolor:any;
	private other: Array<boolean> = [];
	private recent = new Array<Product>();
	private colorterms: any;
	private sizeterms: any;
	private price: any;
	private minprice: any;

  constructor(private navParams: NavParams, public modalCtrl: ModalController, private global: Global, private toastCtrl: ToastController, public navCtrl: NavController, private service: WordpressClient, private loadingCtrl: LoadingController, private alertCtrl: AlertController, public actionSheetController: ActionSheetController) {
		this.colorterms = navParams.get('color');
		this.sizeterms  = navParams.get('size');
		var max = navParams.get('price');
		var min = navParams.get('minprice');
		if(max && max.length>0)
		{
			this.price = Math.max.apply(Math, max);
		}
		else{
			this.price = Number(50000);
		}
		if(min && min.length>0)
			this.minprice = Math.min.apply(Math, min); 
		else
			this.minprice = Number(0); 
		console.log(this.minprice);
		this.wishcolor = '';
		let currency: Currency = JSON.parse(window.localStorage.getItem(Constants.CURRENCY));
		if (currency) {
			this.currencyText = currency.value;
			let iconText = currency.options[currency.value];
			this.currencyIcon = iconText.substring(iconText.lastIndexOf('(') + 1, iconText.length - 1);
		}
			this.presentLoading('Loading products');
		/*wishlist*/
		if ("wishlistproducts" in localStorage) {
		    this.wishlistproducts= JSON.parse(localStorage.getItem('wishlistproducts'));
			this.wishcount= this.wishlistproducts.length;
		}
		else{
			localStorage.setItem("wishlistproducts", '[]');
		}
		
		this.loadProducts();
	}

  ionViewWillLeave() {
		this.subscriptions.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
		this.dismissLoading();
	}

	ionViewDidEnter() {
		this.cartTotal = Number(this.global.getCartItemsCount());
	}
	something(i){
		return i;
	}

	loadProducts() {
		let subscription: Subscription = this.service.productsByfilter(window.localStorage.getItem(Constants.ADMIN_API_KEY), this.colorterms, this.sizeterms, this.price, this.minprice, String(this.productsAllPage)).subscribe(data => {
			this.dismissLoading();
			let products: Array<Product> = data;
			console.log(products);
			this.productsResponse = products;
			let proSplit = new Array<Product>();
			let pindex=0;
			for (let pro of products) {
				/*for wishlist product*/
				pro['index'] = ++pindex;
				if (this.wishlistproducts.some(e => e.id === pro.id)) {
					this.other[pindex] = true;
			 		pro.wishlist= true; 
				}
				else {
					this.other[pindex] = false;
					pro.wishlist= false;	
				}
				/*end*/
				if (pro.type == 'grouped' || pro.type == 'external')
					continue;
				if (proSplit.length == 2) {
					this.productsAll.push(proSplit);
					proSplit = new Array<Product>();
				}
				if (!pro.sale_price) {
					pro.sale_price = pro.regular_price;
				}
				if (this.currencyIcon) {
					pro.regular_price_html = this.currencyIcon + ' ' + pro.regular_price;
					pro.sale_price_html = this.currencyIcon + ' ' + pro.sale_price;
				} else if (this.currencyText) {
					pro.regular_price_html = this.currencyText + ' ' + pro.regular_price;
					pro.sale_price_html = this.currencyText + ' ' + pro.sale_price;
				}
				pro.favorite = this.global.isFavorite(pro);
				proSplit.push(pro);
			}
				
			if (proSplit.length > 0) {
				this.productsAll.push(proSplit);
			}
			this.productsAll = this.productsAll;
			console.log(this.productsAll);
		}, err => {
			this.dismissLoading();
		});
		this.subscriptions.push(subscription);	
	}

	doInfinite(infiniteScroll: any) {
		this.productsAllPage++;
		let subscription: Subscription = this.service.productsByfilter(window.localStorage.getItem(Constants.ADMIN_API_KEY), this.colorterms, this.sizeterms, this.price,this.minprice, String(this.productsAllPage)).subscribe(data => {
			let products: Array<Product> = data;
			this.productsResponse = products;
			let proSplit = new Array<Product>();
			 let pindex=0;
			for (let pro of products) {
				pro['index'] = ++pindex;
				/*for wishlist product*/
				if (this.wishlistproducts.some(e => e.id === pro.id)) {
					this.other[pindex] = true;
			 		pro.wishlist= true; 
				}
				else {
					this.other[pindex] = false;
					pro.wishlist= false;	
				}
				/*end*/
				
				if (pro.type == 'grouped' || pro.type == 'external')
					continue;
				if (proSplit.length == 2) {
					this.productsAll.push(proSplit);
					proSplit = new Array<Product>();
				}
				if (!pro.sale_price) {
					pro.sale_price = pro.regular_price;
				}
				if (this.currencyIcon) {
					pro.regular_price_html = this.currencyIcon + ' ' + pro.regular_price;
					pro.sale_price_html = this.currencyIcon + ' ' + pro.sale_price;
				} else if (this.currencyText) {
					pro.regular_price_html = this.currencyText + ' ' + pro.regular_price;
					pro.sale_price_html = this.currencyText + ' ' + pro.sale_price;
				}
				pro.favorite = this.global.isFavorite(pro);
				proSplit.push(pro);
			}
			if (proSplit.length > 0) {
				this.productsAll.push(proSplit);
			}
			infiniteScroll.complete();
		}, err => {
			infiniteScroll.complete();
			console.log(err);
		});
		this.subscriptions.push(subscription);
		console.log(this.productsAll);
	}

	itemdetailPage(pro) {
		if (localStorage.getItem('recentproducts')!=null) {
			let recentlength= JSON.parse(window.localStorage.getItem('recentproducts'));
			this.recent = recentlength;
			if (this.recent.some(e => e.id === pro.id)) {
			}
			else{
				this.recent.push(pro);
				localStorage.setItem('recentproducts', JSON.stringify(this.recent));
			}
			//console.log(recentlength.length);
		}
		else{
			this.recent.push(pro);
			localStorage.setItem('recentproducts', JSON.stringify(this.recent));
		}
		this.navCtrl.push('ItemdetailPage', { pro: pro, pros: this.productsResponse });
	}

	addToCart(product) {
		if (product.in_stock && product.purchasable) {
			let added: boolean = this.global.addCartItem(product);
			if (added) {
				this.cartTotal = this.cartTotal + 1;
				this.showToast('Item added');
			}
			else{
				this.showToast('item_updated');
			}

			/*this.translate.get(added ? 'item_added' : 'item_updated').subscribe(value => {
				this.showToast(value);
			});*/
		} else {
				this.showToast('Item unavailable to buy.');
		}
	}


	private presentLoading(message: string) {
		this.loading = this.loadingCtrl.create({
			content: message
		});
		this.loading.onDidDismiss(() => { });
		this.loading.present();
		this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

	private presentErrorAlert(msg: string) {
		let alert = this.alertCtrl.create({
			title: 'Error',
			subTitle: msg,
			buttons: ['Dismiss']
		});
		alert.present();
	}

	showToast(message: string) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 3000,
			position: 'bottom'
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});
		toast.present();
	}

	cartPage() {
		let modal = this.modalCtrl.create('CartPage');
		modal.onDidDismiss(() => {
			this.cartTotal = Number(this.global.getCartItemsCount());
		});
		modal.present();
	}

	shortPage() {
		let modal = this.modalCtrl.create('ShortPage');
		modal.present();
	}

	filterPage() {
		let modal = this.modalCtrl.create('FilterPage');
		modal.present();
	}

	wishlistPage() {
		this.navCtrl.push('WishlistPage');
	}

	addtowishlist(pro,index){
		
		if(this.wishcount!=0)
	    { 
	      this.wishlistproducts= JSON.parse(localStorage.getItem('wishlistproducts'));
	    }
	    this.wishlistproducts = this.wishlistproducts || [];
	    /*push into an array & set into localstorage*/
	    if (this.wishlistproducts.some(e => e.id === pro.id)) {
	    	this.showToast('updated to Wishlist');
	    	this.wishcolor = '#fd1d16';
			let pid = this.wishlistproducts.map(obj => obj.id).indexOf(pro.id);
			this.wishlistproducts.splice(pid, 1);
			this.other[index] = false; 
		}
		else {
			this.showToast('Added to Wishlist');
			this.wishcolor = '#999';
			this.wishlistproducts.push(pro);
			this.other[index] = true; 	
		}
	    localStorage.setItem('wishlistproducts', JSON.stringify(this.wishlistproducts));
	    this.wishcount = this.wishlistproducts.length;
	}

	searchPage() {
      this.navCtrl.push('SearchPage');
    }

    /*kk code refresh*/
	refresh(event) {
	    this.refreshproducts();
	    setTimeout(() => {
	      console.log('Async operation has ended');
	      event.complete();
	    }, 2000);
	 }

	refreshproducts()
	{
		let subscription: Subscription = this.service.productsByCate_Cache(window.localStorage.getItem(Constants.ADMIN_API_KEY), this.category.id, "1").subscribe(data => {
		});
	}
	/*end*/

}
