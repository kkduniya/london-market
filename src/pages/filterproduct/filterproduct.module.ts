import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FilterproductPage } from './filterproduct';

@NgModule({
  declarations: [
    FilterproductPage,
  ],
  imports: [
    IonicPageModule.forChild(FilterproductPage),
  ],
})
export class FilterproductPageModule {}
