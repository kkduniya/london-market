import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, AlertController, NavParams, ViewController, ToastController, App, Loading, LoadingController } from 'ionic-angular';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Global } from '../../providers/global';
import { Subscription } from "rxjs/Subscription";
import { Constants } from "../../models/constants.models";

/**
 * Generated class for the AppointmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-appointment',
  templateUrl: 'appointment.html',
  providers: [WordpressClient, Global]
})
export class AppointmentPage {
private policy:any;
private loading: Loading;
private loadingShown: Boolean = false;
private pagedata:any;
private subscriptions: Array<Subscription> = [];
private contact:any={name:'',email:'',mobile:'',info:''};
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private service: WordpressClient ,private global: Global, private loadingCtrl: LoadingController,private toastCtrl: ToastController, private alertCtrl: AlertController) {
  	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentPage');
  }

  goBack() {
	    this.navCtrl.pop();
  }

  sendemail()
  {
  	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  	if (this.contact.name == "" || !this.contact.name.length)
		this.showToast('Name field is empty');
	else if(this.contact.email == "" || !reg.test(this.contact.email))
		this.showToast('email field is empty or not valid');
	else if(this.contact.mobile == "" || this.contact.mobile.length!=10)
		this.showToast('Phone field is empty or not valid');
	else {
		this.presentLoading('Loading products');
		let subscription: Subscription = this.service.insert_feedback(window.localStorage.getItem(Constants.ADMIN_API_KEY),this.contact)
			.subscribe(data => {
				this.dismissLoading();
				this.presentErrorAlert('Thank you, we will contact you shortly');
				this.navCtrl.push('HomePage');
			}, err => {
				this.dismissLoading();
				this.presentErrorAlert('Unable to login with provided credentials');
			});
		this.subscriptions.push(subscription);
	}
  }
  	private presentLoading(message: string) {
		this.loading = this.loadingCtrl.create({
			content: message
		});
		this.loading.onDidDismiss(() => { });
		this.loading.present();
		this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

	private presentErrorAlert(msg: string) {
		let alert = this.alertCtrl.create({
			title: 'London Market',
			subTitle: msg,
			buttons: ['Dismiss']
		});
		alert.present();
	}

	showToast(message: string) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 3000,
			position: 'bottom'
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});
		toast.present();
	}

}
