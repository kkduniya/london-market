import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { AppointmentPage } from './appointment';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [AppointmentPage],
  imports: [
    IonicPageModule.forChild(AppointmentPage),
    IonicImageLoader
  ]
})
export class AppointmentPageModule {}
