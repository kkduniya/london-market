import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { OtpPage } from './otp';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [OtpPage],
  imports: [
    IonicPageModule.forChild(OtpPage),
    IonicImageLoader
  ]
})
export class OtpPageModule {}
