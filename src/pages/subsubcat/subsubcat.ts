import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, NavParams } from 'ionic-angular';
import { Category } from "../../models/category.models";
import { Constants } from "../../models/constants.models";
/**
 * Generated class for the SubsubcatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subsubcat',
  templateUrl: 'subsubcat.html',
})
export class SubsubcatPage {
private category: Category;
private catsToShow: Array<Category>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.catsToShow = navParams.get('cat');   
    console.log(this.catsToShow);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubcategoryPage');
  }

  shirtsPage(cat: Category) {
		if (cat.id != '-1') {
			this.navCtrl.push('ShirtsPage', { cat: cat });
		}
	}

  searchPage() {
      this.navCtrl.push('SearchPage');
      // let modal = this.modalCtrl.create(SearchPage);
      // modal.present();
    }

}
