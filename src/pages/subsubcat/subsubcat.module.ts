import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubsubcatPage } from './subsubcat';

@NgModule({
  declarations: [
    SubsubcatPage,
  ],
  imports: [
    IonicPageModule.forChild(SubsubcatPage),
  ],
})
export class SubsubcatPageModule {}
