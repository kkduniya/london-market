import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SizechartPage } from './sizechart';

@NgModule({
  declarations: [
    SizechartPage,
  ],
  imports: [
    IonicPageModule.forChild(SizechartPage),
  ],
})
export class SizechartPageModule {}
