import { Component } from '@angular/core';
import { IonicPage, IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the SizechartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sizechart',
  templateUrl: 'sizechart.html',
})
export class SizechartPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SizechartPage');
  }

  dismisspage() {
	this.viewCtrl.dismiss();
  }

}
