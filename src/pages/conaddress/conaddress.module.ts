import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { ConaddressPage } from './conaddress';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [ConaddressPage],
  imports: [
    IonicPageModule.forChild(ConaddressPage),
    IonicImageLoader
  ]
})
export class ConaddressPageModule {}
