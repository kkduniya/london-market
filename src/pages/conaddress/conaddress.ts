import { Component, Inject } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, NavParams, ToastController, Loading, LoadingController } from 'ionic-angular';
import { Address } from '../../models/address.models';
import { Product } from '../../models/product.models';
import { Constants } from '../../models/constants.models';
import { ShippingZoneLocation } from '../../models/shipping-zone-location.models';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Subscription } from 'rxjs/Subscription';
import { ShippingMethod } from '../../models/shipping-method.models';
import { APP_CONFIG, AppConfig } from '../../app/app.config';

@IonicPage()
@Component({
  selector: 'page-conaddress',
  templateUrl: 'conaddress.html',
  providers: [WordpressClient]
})

export class ConaddressPage {
  private shippingMethods: Array<ShippingMethod>;
  private product: Product;
  private selectedAddress: Address;
  private addressChangeText = 'Change';
  private loading: Loading;
  private loadingShown: Boolean = false;
  private subscriptions: Array<Subscription> = [];
  private selection = -1;

  constructor(private navCtrl: NavController,
    private navParams: NavParams, private toastCtrl: ToastController,
    private loadingCtrl: LoadingController, private service: WordpressClient) {
    this.product = this.navParams.get('pro');
    window.localStorage.removeItem(Constants.SELECTED_SHIPPING_METHOD);
  }

  ionViewWillLeave() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.dismissLoading();
  }

  ionViewDidEnter() {
    this.selectedAddress = JSON.parse(window.localStorage.getItem(Constants.SELECTED_ADDRESS));
    if(this.selectedAddress == null)
    {
      this.addressChangeText = 'Add';
    }
    else{
      this.addressChangeText = 'Change';
    }

    this.checkShipping();
  }

  checkShipping() {
    let shippingZoneLocation = this.matchZone();
    console.log('szl match', shippingZoneLocation);
    if (shippingZoneLocation) {
        this.presentLoading('Fetching shipping methods');
      let subscription: Subscription = this.service.shippingMethods(window.localStorage.getItem(Constants.ADMIN_API_KEY), shippingZoneLocation.zoneId).subscribe(data => {
        this.dismissLoading();
        this.shippingMethods = data;
      }, err => {
        console.log(err);
        this.dismissLoading();
      });
      this.subscriptions.push(subscription);
    }
  }

  matchZone(): ShippingZoneLocation {
    let matched: ShippingZoneLocation;
    let shippingZoneLocations: Array<ShippingZoneLocation> = JSON.parse(window.localStorage.getItem(Constants.SHIPPING_ZONE_LOCATIONS));
    if (shippingZoneLocations) {
      for (let szl of shippingZoneLocations) {
        if (szl.type == "postcode") {
          if (szl.code.toLocaleLowerCase().includes(this.selectedAddress.postcode.toLocaleLowerCase()) || this.selectedAddress.postcode.toLocaleLowerCase().includes(szl.code.toLocaleLowerCase())) {
            matched = szl;
            break;
          }
          if (szl.code.toLocaleLowerCase().includes(".")) {
            let code = szl.code.split(".");
            if (code && code.length >= 4) {
              let min = code[0];
              let max = code[3].trim();
              if (Number(this.selectedAddress.postcode) >= Number(min) && Number(this.selectedAddress.postcode) <= Number(max)) {
                matched = szl;
                break;
              }
            }
          }
        }
      }
      if (!matched && this.selectedAddress && this.selectedAddress.country) {

        for (let szl of shippingZoneLocations) {
          if (szl.type == "country") {
            if (szl.code == "IN" && this.selectedAddress.country.toLowerCase().indexOf('india') > -1) {
              matched = szl;
              break;
            }
            if (this.selectedAddress.country == szl.code) {
              matched = szl;
              break;
            }
          }
        }
      }
    }
    return matched;
  }

  setDefaultShipping(sm: ShippingMethod) {
    this.selection = sm.id;
    console.log("sm", sm);
    if (sm)
      window.localStorage.setItem(Constants.SELECTED_SHIPPING_METHOD, JSON.stringify(sm));
    else
      window.localStorage.removeItem(Constants.SELECTED_SHIPPING_METHOD);

    this.confirm();
  }

  addressPage() {
    this.navCtrl.push('AddressSelectPage', { action: 'choose' });
  }

  confirm() {
    if (this.selectedAddress == null) {
        this.showToast('Please select an address.');
    } else {
      if (this.product)
        this.navCtrl.push('ShippiningPage', { pro: this.product });
      else
        this.navCtrl.push('ShippiningPage');
    }
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

  private presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message
    });

    this.loading.onDidDismiss(() => { });

    this.loading.present();
    this.loadingShown = true;
  }

  private dismissLoading() {
    if (this.loadingShown) {
      this.loadingShown = false;
      this.loading.dismiss();
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

}