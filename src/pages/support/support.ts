import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

/**
 * Generated class for the SupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
})
export class SupportPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportPage');
  }

  call(phoneNumber)
  {
  	window.open('tel:' + phoneNumber, '_system');
  }

  openemail(email){
  	window.open('mailto:' + email, '_system');
  }

policymodal(){
    let modal = this.modalCtrl.create('PolicyPage');
    modal.onDidDismiss(() => {
    });
    modal.present();
  }

}
