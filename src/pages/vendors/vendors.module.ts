import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { VendorsPage } from './vendors';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [VendorsPage],
  imports: [
    IonicPageModule.forChild(VendorsPage),
    IonicImageLoader
  ]
})
export class VendorsPageModule {}
