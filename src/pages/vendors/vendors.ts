import { Component } from '@angular/core';
import { NavController, IonicPage,IonicPageModule, NavParams, Loading, LoadingController, ToastController} from 'ionic-angular';
import { Global } from '../../providers/global';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Constants } from "../../models/constants.models";
import { Subscription } from "rxjs/Subscription";
import { Product } from "../../models/product.models";
import { Currency } from "../../models/currency.models";
import { Category } from "../../models/category.models";
import { Image } from '../../models/image.models';
/**
 * Generated class for the VendorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-vendors',
  templateUrl: 'vendors.html',
  providers: [WordpressClient,Global]
})
export class VendorsPage {
 private subscriptions: Array<Subscription> = [];
 private productsAll = new Array<Array<Product>>();
 private productsResponse = new Array<Product>();
 private pageno: any = 1;
 private loading: Loading;
 private refreshing: boolean;
 private loadingShown: Boolean = false;
 private vendors:any;
 private products: Array<Product>;
 constructor(public navCtrl: NavController, public navParams: NavParams, private service: WordpressClient,private toastCtrl: ToastController, private loadingCtrl: LoadingController, private global: Global) {
  	this.presentLoading('Loading..');
  	this.getallvendors();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad vendorsPage');
  }

  getallvendors()
  {
    let subscription: Subscription = this.service.getAllVendor(window.localStorage.getItem(Constants.ADMIN_API_KEY)).
    subscribe(data => {
     	this.dismissLoading();
     	console.log(data);
     	this.vendors = data;
     	console.log(this.vendors);
		}, err => {
			this.dismissLoading();
		});
		this.subscriptions.push(subscription);
  }

  /*kk code refresh*/
	refresh(event) {
	    this.getvendorsall();
	    setTimeout(() => {
	      console.log('Async operation has ended');
	      event.complete();
	    }, 2000);
	 }

	getvendorsall(){
		let subscription: Subscription = this.service.getAllVendorwocache(window.localStorage.getItem(Constants.ADMIN_API_KEY)).
    	subscribe(data => {
     	this.dismissLoading();
     	console.log(data);
     	this.vendors = data;
     	console.log(this.vendors);
		}, err => {
			this.dismissLoading();
		});
		this.subscriptions.push(subscription);
	}

	productsbyvendors(vid){
		this.navCtrl.push('VendorsproductPage', {vid: vid});
	}

	private presentLoading(message: string) {
		this.loading = this.loadingCtrl.create({
			content: message
		});
		this.loading.onDidDismiss(() => { });
		this.loading.present();
		this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

}
