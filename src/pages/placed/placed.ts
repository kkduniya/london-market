import { Component } from '@angular/core';
import { NavController ,IonicPage,IonicPageModule} from 'ionic-angular';


@IonicPage()
@Component({
	selector: 'page-placed ',
	templateUrl: 'placed.html'
})
export class PlacedPage {

	constructor(public navCtrl: NavController) { }

	ordersPage() {
		this.homePage();
		this.navCtrl.push('Myorder_2Page');
	}

	homePage() {
		this.navCtrl.setRoot('HomePage');
	}

}
