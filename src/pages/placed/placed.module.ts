import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { PlacedPage } from './placed';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [PlacedPage],
  imports: [
    IonicPageModule.forChild(PlacedPage),
    IonicImageLoader
  ]
})
export class PlacedPageModule {}
