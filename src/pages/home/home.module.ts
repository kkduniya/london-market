import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { IonicImageLoader } from 'ionic-image-loader';
import { HideHeaderDirective } from '../../directives/hide-header/hide-header';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [HomePage, HideHeaderDirective],
  imports: [
    IonicPageModule.forChild(HomePage),
    IonicImageLoader,
    IonicImageViewerModule,
  ]
})
export class HomePageModule {}
