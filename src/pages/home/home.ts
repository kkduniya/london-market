import { Component, Inject } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, ModalController, MenuController, Events, Platform, NavParams, ViewController } from 'ionic-angular';

import { Global } from '../../providers/global';
import { UserResponse } from "../../models/user-response.models";
import { Category } from "../../models/category.models";
import { Constants } from "../../models/constants.models";
import { CartItem } from "../../models/cart-item.models";
import { AppConfig, APP_CONFIG } from '../../app/app.config';
import { Subscription } from '../../../node_modules/rxjs/Subscription';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Banner } from '../../models/banner.models';
import { Product } from '../../models/product.models';
import { IonicImageLoader } from 'ionic-image-loader';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [Global, WordpressClient]
})

export class HomePage {
  private subscriptions: Array<Subscription> = [];
  private banners = new Array<Banner>();
  private categoriesAll = new Array<Category>();
  private cartTotal = 0;
  private appTitle;
  private user: UserResponse;
  private catid = 62;
  private productsAllPage: number = 1;
  private productsAll:any = [];
  private homecatobj:any=[];
  private userimage:any;
  private tag :any;
  private jewellery:any=[];
  private profile: any;
  
  constructor(@Inject(APP_CONFIG) private config: AppConfig, private navParams: NavParams, private events: Events, private service: WordpressClient, public modalCtrl: ModalController, public navCtrl: NavController, public menu: MenuController, private global: Global, private viewCtrl: ViewController) {
    this.appTitle = config.appName;
    events.subscribe('category:setup', () => {
      this.setupCategories();
    });

    this.setupCategories();
    this.loadBanners();
    this.productsection();
    this.user = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));
    if(this.user && this.user.meta_data !=null && this.user.meta_data!=undefined)
    {
      let profilemeta = this.user.meta_data;

      if(this.user && this.user.meta_data !=null && this.user.meta_data.length > 0)
      { /*this.profile = navParams.get('profile');*/
        for(let i=0; i<this.user.meta_data.length; i++)
        {
          if(profilemeta[i].key=='pimage')
          {
            this.profile = profilemeta[i].value;
          }
        }
      }
    }

    let toOpen: string = window.localStorage.getItem(Constants.TEMP_OPEN);
      if (this.user && toOpen && toOpen.length) {
      if (toOpen == Constants.TEMP_OPEN_CART) {
        this.navCtrl.push('ConaddressPage');
      } else if (toOpen == Constants.TEMP_OPEN_PRODUCT) {
        let product: Product = JSON.parse(window.localStorage.getItem(Constants.TEMP_OPEN_PRODUCT));
        this.navCtrl.push('ConaddressPage', { pro: product });
      }

      window.localStorage.removeItem(Constants.TEMP_OPEN);
      window.localStorage.removeItem(Constants.TEMP_OPEN_CART);
      window.localStorage.removeItem(Constants.TEMP_OPEN_PRODUCT);
    }
  }

  setupCategories() {
    let categories: Array<Category> = JSON.parse(window.localStorage.getItem(Constants.PRODUCT_CATEGORIES_PARENT));
    let cats = new Array<Category>();
    if(categories && categories.length>0){
    for (let cat of categories) {
      if (cats.length == 15) {
        break;
      }
      if(cat.id=='62' || cat.id=='59'){
        this.homecatobj.push(cat);
      }
      cats.push(cat);
    }
    /*more*/
      /*let more = new Category();
      more.name = 'More';
      more.id = '-1';
      cats.push(more);*/
      this.categoriesAll = cats;
    }
  }

  ionViewDidEnter() {
    this.cartTotal = Number(this.global.getCartItemsCount());

    this.user = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));

    if(this.user && this.user.meta_data !=null && this.user.meta_data!=undefined)
    {
      let profilemeta = this.user.meta_data;

      if(this.user && this.user.meta_data !=null && this.user.meta_data.length > 0)
      { 
        for(let i=0; i<this.user.meta_data.length; i++)
        {
          if(profilemeta[i].key=='pimage')
          {
            this.profile = profilemeta[i].value;
          }
        }
      }
    }

  }

  loadBanners() {
    let savedBanners: Array<Banner> = JSON.parse(window.localStorage.getItem('banners'));
    if (savedBanners && savedBanners!=null) {
      this.banners = savedBanners;
    }
    else{
      let subscription: Subscription = this.service.banners(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {
        let banners: Array<Banner> = data;
        let categories: Array<Category> = JSON.parse(window.localStorage.getItem(Constants.PRODUCT_CATEGORIES));
        for (let ban of banners) {
          for (let cat of categories) {
            if (cat.id == ban.category) {
              ban.catObj = cat;
              break;
            }
          }
        }
        this.banners = banners;
        window.localStorage.setItem('banners', JSON.stringify(this.banners));
      }, err => {
      });
      this.subscriptions.push(subscription);
    }
  }

  menuToggle() {
    if (!this.menu.isEnabled()) {
      this.menu.enable(true);
      this.menu.swipeEnable(true);
    }
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      this.menu.open();
    }
  }

  Category_Page(cats) {
    console.log(cats);
    let categories: Array<Category> = JSON.parse(window.localStorage.getItem(Constants.PRODUCT_CATEGORIES));
      let parentWithChild = new Array<Category>();
          for (let cat of categories) {
            if (cat.parent == cats) {
              parentWithChild.push(cat);
            }
          }
    if (parentWithChild && parentWithChild!=null){
      this.navCtrl.push('SubcategoryPage', { cat: parentWithChild });
    }
    else{
      this.navCtrl.push('CategoryPage');
    }
  }

 categoryall(cats){
  this.navCtrl.push('CategoryPage');
 }

 accountPage(){
    this.user = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));
    if(this.user)
    this.navCtrl.push('ProfilePage');
    else
    this.navCtrl.push('LoginPage');
  }

  searchPage() {
    this.navCtrl.push('SearchPage');
    // let modal = this.modalCtrl.create(SearchPage);
    // modal.present();
  }

  shopPage()
  {
    this.navCtrl.push('CategoryPage');
  }

  productsection(){
    let subscription: Subscription = this.service.productsByCategory(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.catid), String(this.productsAllPage)).subscribe(data => {  
          this.productsAll = data;
    }, err => {
      console.log(err);
    });
  }

  cartPage() {
    //this.navCtrl.push(CartPage);
    let modal = this.modalCtrl.create('CartPage');
    modal.onDidDismiss(() => {
      this.cartTotal = Number(this.global.getCartItemsCount());
    });
    modal.present();
  }

  itemdetailPage(pro) {
    this.navCtrl.push('ItemdetailPage', { pro: pro, pros: ''});
  }

 /*vendorPage(vid) {
    this.navCtrl.push(VendorsproductPage, {vid: vid});
  }*/

  productbycity(tid)
  {
    this.navCtrl.push('TagproductPage', {tag_id: tid});
  }
  
  shirtsPage(cat: Category) {
    if (cat.id != '-1') {
      this.navCtrl.push('ShirtsPage', { cat: cat });
    }
  }

  exhibition() {
      this.navCtrl.push('ExhibitionPage');
  }

  tagspage(id) {
      this.navCtrl.push('TagproductPage',{tag_id:id});
  }

  wishlistPage() {
      this.navCtrl.push('WishlistPage');
  }

  justinpage() {
      this.navCtrl.push('JustinPage');
  }

  seasions(){
      this.navCtrl.push('SeasonsPage');
  }

  designers(){
      this.navCtrl.push('FocusPage');
  }

  madeonorder(){
      this.navCtrl.push('TagproductPage', {tag_id:340});
  }

  refresh(event) {
      this.update_userkey();
      setTimeout(() => {
        console.log('Async operation has ended');
        event.complete();
      }, 2000);
   }

  update_userkey()
  {
    this.user = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));
    if(this.user && this.user.meta_data !=null && this.user.meta_data!=undefined)
    {
      let profilemeta = this.user.meta_data;

      if(this.user && this.user.meta_data !=null && this.user.meta_data.length > 0)
      {
        for(let i=0; i<this.user.meta_data.length; i++)
        {
          if(profilemeta[i].key=='pimage')
          {
            this.profile = profilemeta[i].value;
          }
        }
      }
    }
  }
  /*productstag(){
    let subscription: Subscription = this.service.productstag(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {  
          this.tag = data;
    }, err => {
      console.log(err);
    });
    this.subscriptions.push(subscription);
  }*/

  slides = [
    {
      title: "20% Off",
      description: "Furits & Veggies",
      smalltext: "Fresh & healthy",
      image: "assets/imgs/slider-1.jpg",
    },
    {
      title: "20% Off",
      description: "Tops & Tunics",
      smalltext: "Fresh & healthy",
      image: "assets/imgs/slider-2.jpg",
    },
    {
      title: "20% Off",
      description: "Tops & Tunics",
      smalltext: "Fresh & healthy",
      image: "assets/imgs/slider-3.jpg",
    }
  ];

  homeicons = [
    {
      name: "Vegetables & Fruits",
      imag: "assets/imgs/1.png",
    },
    {
      name: "Bakery & Dairy Products",
      imag: "assets/imgs/bakery.png",
    },
    {
      name: "Foodgrains, oil & masaala",
      imag: "assets/imgs/foodgrains.png",
    },
    {
      name: "Bevrages & drinks",
      imag: "assets/imgs/beverages.png",
    },
    {
      name: "Branded foods products",
      imag: "assets/imgs/branded.png",
    },
    {
      name: "Beauty & hygiene",
      imag: "assets/imgs/beauty.png",
    },
    {
      name: "Fish, Meet & Eggs",
      imag: "assets/imgs/non-veg.png",
    },
    {
      name: "Household products",
      imag: "assets/imgs/hosehold.png",
    },
    {
      name: "Grument & world food",
      imag: "assets/imgs/gourmet.png",
    }
  ];

}
