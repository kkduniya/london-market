import { Component } from '@angular/core';
import { NavController, ModalController, IonicPage,IonicPageModule } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-myorder_1 ',
  templateUrl: 'myorder_1.html'
})
export class Myorder_1Page {

 account: string = "profile";
 constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

  }
  
  searchPage() {
    let modal = this.modalCtrl.create('SearchPage');
    modal.present();
  }
  
    cartPage() {
    let modal = this.modalCtrl.create('CartPage');
    modal.present();
  }

}
