import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { Myorder_1Page } from './myorder_1';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [Myorder_1Page],
  imports: [
    IonicPageModule.forChild(Myorder_1Page),
    IonicImageLoader
  ]
})
export class Myorder_1PageModule {}
