import { Component } from '@angular/core';
import { NavController, IonicPage,IonicPageModule, NavParams, ViewController, ToastController, App } from 'ionic-angular';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Global } from '../../providers/global';
import { Subscription } from "rxjs/Subscription";
import { Constants } from "../../models/constants.models";
/**
 * Generated class for the PolicyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-policy',
  templateUrl: 'policy.html',
  providers: [WordpressClient, Global]
})
export class PolicyPage {
private policy:any;
private pageid = 498;
private pagedata:any;
private subscriptions: Array<Subscription> = [];
    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private service: WordpressClient ,private global: Global) {
  	this.getpagecontent();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PolicyPage');
  }

  dismiss() {
		this.viewCtrl.dismiss();
	}

  getpagecontent()
  {
    let subscription: Subscription = this.service.getPage(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.pageid)).
    subscribe(data => {
      this.pagedata = data;
    }, err => {
      this.pagedata = err;
    });
    this.subscriptions.push(subscription);
  }

}
