import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { PolicyPage } from './policy';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [PolicyPage],
  imports: [
    IonicPageModule.forChild(PolicyPage),
    IonicImageLoader
  ]
})
export class PolicyPageModule {}
