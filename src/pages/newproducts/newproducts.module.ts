import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { NewproductsPage } from './newproducts';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [NewproductsPage],
  imports: [
    IonicPageModule.forChild(NewproductsPage),
    IonicImageLoader
  ]
})
export class NewproductsPageModule {}
