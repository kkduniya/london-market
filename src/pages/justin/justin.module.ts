import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { JustinPage } from './justin';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [JustinPage],
  imports: [
    IonicPageModule.forChild(JustinPage),
    IonicImageLoader
  ]
})
export class JustinPageModule {}
