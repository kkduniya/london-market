import { Component } from '@angular/core';
import { NavController, IonicPage,IonicPageModule, NavParams, Loading, LoadingController, ToastController} from 'ionic-angular';
import { Global } from '../../providers/global';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Constants } from "../../models/constants.models";
import { Subscription } from "rxjs/Subscription";
import { Product } from "../../models/product.models";
import { Currency } from "../../models/currency.models";
import { Category } from "../../models/category.models";
import { Image } from '../../models/image.models';


/**
 * Generated class for the JustinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-justin',
  templateUrl: 'justin.html',
  providers: [WordpressClient, Global]
})
export class JustinPage {
 private subscriptions: Array<Subscription> = [];
 private productsAll = new Array<Array<Product>>();
 private productsResponse = new Array<Product>();
 private currencyIcon: string;
 private currencyText: string;
 private loading: Loading;
 private loadingShown: Boolean = false;
 private product: Product;
 private pageno: number = 1;
 private justincat: number = 62;
  constructor(public navCtrl: NavController, public navParams: NavParams, private service: WordpressClient,private toastCtrl: ToastController, private global: Global, private loadingCtrl: LoadingController) {
  	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JustinPage');
    
  }
	
  vendorspage(){
  	this.navCtrl.push('VendorsPage');
  } 

  newproductspage() {
	 this.navCtrl.push('NewproductsPage');
  }

	private presentLoading(message: string) {
	this.loading = this.loadingCtrl.create({
		content: message
	});

	this.loading.onDidDismiss(() => { });

	this.loading.present();
	this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

	goBack() {
		this.navCtrl.push('HomePage');
	}

}
