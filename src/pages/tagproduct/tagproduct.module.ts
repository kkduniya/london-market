import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { TagproductPage } from './tagproduct';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [TagproductPage],
  imports: [
    IonicPageModule.forChild(TagproductPage),
    IonicImageLoader
  ]
})
export class TagproductPageModule {}
