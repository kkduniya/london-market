import { Component } from '@angular/core';
import { NavController, IonicPage,IonicPageModule, NavParams, Loading, LoadingController, ToastController} from 'ionic-angular';
import { Global } from '../../providers/global';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Constants } from "../../models/constants.models";
import { ItemdetailPage } from '../itemdetail/itemdetail';
import { Subscription } from "rxjs/Subscription";
import { Product } from "../../models/product.models";
import { Currency } from "../../models/currency.models";
import { Category } from "../../models/category.models";
import { Image } from '../../models/image.models';

/**
 * Generated class for the AllbrandPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-allbrand',
  templateUrl: 'allbrand.html',
  providers: [WordpressClient,Global]
})
export class AllbrandPage {
 private subscriptions: Array<Subscription> = [];
 private productsAll = new Array<Array<Product>>();
 private productsResponse = new Array<Product>();
 private pageno: any = 1;
 private loading: Loading;
 private loadingShown: Boolean = false;
 private tags:any;
 private products: Array<Product>;
 private sorttags:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private service: WordpressClient, private toastCtrl: ToastController, private loadingCtrl: LoadingController, private global: Global) {
  	this.presentLoading('Loading..');
  	this.getalltags();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AllbrandPage');
  }

  getalltags()
  {
    let subscription: Subscription = this.service.productstag(window.localStorage.getItem(Constants.ADMIN_API_KEY)).
    subscribe(data => {
     	this.dismissLoading();
     	this.tags = data;
     	let sorttag = [];
     	console.log(this.tags.length);
     	for(let i=0; i<this.tags.length; i++)
     	{	
	     	if(String(this.tags[i].name).length==1)
	     	{
	     	  sorttag.push(this.tags[i]);
	     	}
 		}	
 		this.sorttags = sorttag;
 		let abc = this.transform(this.sorttags);
     	console.log(abc);
		}, err => {
			this.dismissLoading();
		});
		this.subscriptions.push(subscription);
  }

/*for sorting*/
  transform(array: Array<any>): Array<string> {
    array.sort((a: any, b: any) => {
      if (a.name < b.name) {
        return -1;
      } else if (a.name > b.name) {
        return 1;
      } else {
        return 0;
      }
    });
    return array;
  }

	productsbytag(tag){
		this.navCtrl.push('TagsPage', {tagid: tag});
	}

	private presentLoading(message: string) {
		this.loading = this.loadingCtrl.create({
			content: message
		});
		this.loading.onDidDismiss(() => { });
		this.loading.present();
		this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

  goBack() {
    this.navCtrl.push('HomePage');
  }

}
