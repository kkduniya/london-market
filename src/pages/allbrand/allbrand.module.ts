import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { AllbrandPage } from './allbrand';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [AllbrandPage],
  imports: [
    IonicPageModule.forChild(AllbrandPage),
    IonicImageLoader
  ]
})
export class AllbrandPageModule {}
