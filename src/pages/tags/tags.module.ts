import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { TagsPage } from './tags';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [TagsPage],
  imports: [
    IonicPageModule.forChild(TagsPage),
    IonicImageLoader
  ]
})
export class TagsPageModule {}
