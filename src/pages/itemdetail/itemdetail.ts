import { Component, Inject } from '@angular/core';
import { NavController, IonicPage,IonicPageModule,AlertController, Loading, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';

import { Global } from '../../providers/global';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Subscription } from "rxjs/Subscription";
import { Product } from "../../models/product.models";
import { Review } from "../../models/review.models";
import { Currency } from "../../models/currency.models";
import { Constants } from "../../models/constants.models";
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { UserResponse } from '../../models/user-response.models';
import { Image } from '../../models/image.models';
import { AppConfig, APP_CONFIG } from '../../app/app.config';

@IonicPage()
@Component({
	selector: 'page-itemdetail ',
	templateUrl: 'itemdetail.html',
	providers: [WordpressClient, Global]
})

export class ItemdetailPage {
	private loading: Loading;
	private loadingShown: Boolean = false;
	private subscriptions: Array<Subscription> = [];
	private product: Product;
	private firstvariation: Product;
	private details: boolean = false;
	private reviews: Array<Review>;
	private productsResponse = new Array<Product>();
	private productVariations = new Array<Product>();
	private imageToDisplay: string;
	private currencyIcon: string;
	private currencyText: string;
	private cartTotal = 0;
	public  description: any;
	private wishlistproducts= [];
	private wishcount:any;
	private wishcolor:any;
	private other:any;
	private vendors:any;
	private pid : any;
	private made_on_order_id : any;
	private variationSelected = 0;
	private sizechart: any;
	private checkmoo: any;
	private recent = new Array<Array<Product>>();
	constructor(@Inject(APP_CONFIG) private config: AppConfig, private socialSharing: SocialSharing, public navCtrl: NavController, private toastCtrl: ToastController, public modalCtrl: ModalController, private global: Global, private navParams: NavParams, private service: WordpressClient, private loadingCtrl: LoadingController, private alertCtrl: AlertController) {
		let currency: Currency = JSON.parse(window.localStorage.getItem(Constants.CURRENCY));
		if (currency) {
			this.currencyText = currency.value;
			let iconText = currency.options[currency.value];
			this.currencyIcon = iconText.substring(iconText.lastIndexOf('(') + 1, iconText.length - 1);
		}
		console.log('abcd');
		this.product = this.navParams.get('pro');
		console.log(this.product);
		/*this.made_on_order_id = this.navParams.get('msg');*/
		
		/*check category Made on order tag exits*/
		if (this.product && this.product.tags.length>0 && this.product.tags.some(e => e.id == '340')) {
		 this.checkmoo= 340;
		}
		else{
			this.checkmoo= 0;
		}
		this.recent = JSON.parse(localStorage.getItem('recentproducts'));
		if (this.product) {
			this.product.favorite = global.isFavorite(this.product);
			let productsResponse: Array<Product> = this.navParams.get('pros');
			for (let pro of productsResponse) {
				if (pro.id != this.product.id) {
					pro.favorite = global.isFavorite(pro);
					this.productsResponse.push(pro);
				}
			}
			if (this.product.images && this.product.images.length) {
				this.imageToDisplay = this.product.images[0].src;
			}
			if (this.product.type == 'variable') {
				this.loadVariations();
			}

			if(this.product && this.product.vendor)
			{
			    let subscription: Subscription = this.service.get_vendorby_id(window.localStorage.getItem(Constants.ADMIN_API_KEY), this.product.vendor).
			    subscribe(data => {
			     	this.dismissLoading();
			     	this.vendors = data;
					}, err => {
						this.dismissLoading();
					});
					this.subscriptions.push(subscription);
			}
			this.loadReviews();
		} else {
			this.loadProductById(this.navParams.get('pro_id'));
		}

	}

	ionViewWillLeave() {
		this.subscriptions.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
		this.dismissLoading();
	}

	ionViewDidEnter() {
		this.cartTotal = Number(this.global.getCartItemsCount());
	}

	loadProductById(proId) {
			this.presentLoading('Loading products');
		let subscription: Subscription = this.service.productById(window.localStorage.getItem(Constants.ADMIN_API_KEY), proId).subscribe(data => {
			this.product = data;
			console.log(this.product);
			this.product.favorite = this.global.isFavorite(this.product);
			if (this.product.images && this.product.images.length) {
				this.imageToDisplay = this.product.images[0].src;
			}
			if (this.currencyIcon) {
				this.product.regular_price_html = this.currencyIcon + ' ' + this.product.regular_price;
				this.product.sale_price_html = this.currencyIcon + ' ' + this.product.sale_price;
			} else if (this.currencyText) {
				this.product.regular_price_html = this.currencyText + ' ' + this.product.regular_price;
				this.product.sale_price_html = this.currencyText + ' ' + this.product.sale_price;
			}
			if (this.product.sale_price_html.length == 0) {
				this.product.sale_price_html = this.product.regular_price_html;
			}
			this.loadReviews();
			this.dismissLoading();
			if (this.product.type == 'variable') {
				this.loadVariations();
			}
		}, err => {
			this.dismissLoading();
		});
		this.subscriptions.push(subscription);
	}

	loadVariations() {
			this.presentLoading('Loading variations');
		let subscription: Subscription = this.service.productVariations(window.localStorage.getItem(Constants.ADMIN_API_KEY), this.product.id).subscribe(data => {
			let variations: Array<Product> = data;
			/*kk code*/
			this.pid = data[0].id;
			/*end*/
			console.log(data);
			for (let vari of variations) {
				let variAttris = '';
				for (let i = 0; i < vari.attributes.length; i++) {
					let attri = vari.attributes[i].name + ' ' + vari.attributes[i].option + (i < vari.attributes.length - 1 ? ', ' : '');
					variAttris = variAttris + attri;
				}

				vari.name = this.product.name + ' - ' + variAttris;
				vari.type = 'variable';
				vari.images = new Array<Image>();
				vari.images.push(vari.image);

				if (!vari.sale_price) {
					vari.sale_price = vari.regular_price;
				}
				if (this.currencyIcon) {
					vari.regular_price_html = this.currencyIcon + ' ' + vari.regular_price;
					vari.sale_price_html = this.currencyIcon + ' ' + vari.sale_price;
				} else if (this.currencyText) {
					vari.regular_price_html = this.currencyText + ' ' + vari.regular_price;
					vari.sale_price_html = this.currencyText + ' ' + vari.sale_price;
				}
			}
			this.productVariations = variations;
			this.firstvariation = this.productVariations[0];
			this.dismissLoading();
		}, err => {
		});
		this.subscriptions.push(subscription);
	}

	showImage(src) {
		this.imageToDisplay = src;
	}

	loadReviews() {
		let subscription: Subscription = this.service.productsReviews(window.localStorage.getItem(Constants.ADMIN_API_KEY), this.product.id).subscribe(data => {
			let reviews: Array<Review> = data;
			let approved = new Array<Review>();
			for (let rev of reviews) {
				if (rev.verified) {
					approved.push(rev);
				}
			}
			this.reviews = approved;
		}, err => {
		});
		this.subscriptions.push(subscription);
	}

	itemdetail(pro) {
		this.navCtrl.push('ItemdetailPage', { pro: pro, pros: this.productsResponse });
	}

	viewMore() {
		this.details = true;
	}

	viewLess() {
		this.details = false;
	}

	toggleFavorite(pro) {
		pro.favorite = this.global.toggleFavorite(pro);
	}

	/*shareProduct(pro) {
		this.socialSharing.share('Found this product on ' + this.config.appName, pro.name, null, pro.permalink).then((data) => {
			console.log(data);
		}).catch((err) => {
			console.log(err);
		});
	}*/

	addToCart() {
		if (this.product.in_stock && this.product.purchasable) {
			let added: boolean = this.global.addCartItem(this.product);
			if (added) {
				this.cartTotal = this.cartTotal + 1;
				this.showToast('Item added');
			}
			else{
				this.showToast('Item updated');
			}
			/*this.translate.get(added ? 'item_added' : 'item_updated').subscribe(value => {
				this.showToast(value);
			});*/
		} else {
				this.showToast('Item unavailable to buy.');
		}
	}

	buyNow() {
		let user: UserResponse = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));
		if (user != null) {
			this.navCtrl.push('ConaddressPage', { pro: this.product });
		} else {
			window.localStorage.setItem(Constants.TEMP_OPEN, Constants.TEMP_OPEN_PRODUCT);
			window.localStorage.setItem(Constants.TEMP_OPEN_PRODUCT, JSON.stringify(this.product));
			this.showToast('Sign in to continue');
			this.navCtrl.push('LoginPage');
			/*this.navCtrl.push(CartPage);*/
			
		}
	}

	buyVariation(variation) {
		let user: UserResponse = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));
		if (user != null) {
			this.navCtrl.push('ConaddressPage', { pro: variation });
		} else {
			window.localStorage.setItem(Constants.TEMP_OPEN, Constants.TEMP_OPEN_PRODUCT);
			window.localStorage.setItem(Constants.TEMP_OPEN_PRODUCT, JSON.stringify(variation));
			this.showToast('Sign in to continue');
			this.navCtrl.push('LoginPage');
		}
	}

	addVariation(variation) {
		if (variation.in_stock && variation.purchasable) {
			let added: boolean = this.global.addCartItem(variation);
			if (added) {
				this.cartTotal = this.cartTotal + 1;
				this.showToast('Item added');
			}
			else{
				this.showToast('Item updated');
			}
			/*this.translate.get(added ? 'item_added' : 'item_updated').subscribe(value => {
				this.showToast(value);
			});*/
		} else {
				this.showToast('Sign in to continue');
				this.navCtrl.push('LoginPage');
		}
	}

	private presentLoading(message: string) {
		this.loading = this.loadingCtrl.create({
			content: message
		});

		this.loading.onDidDismiss(() => { });

		this.loading.present();
		this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

	private presentErrorAlert(msg: string) {
		let alert = this.alertCtrl.create({
			title: 'Error',
			subTitle: msg,
			buttons: ['Dismiss']
		});
		alert.present();
	}

	showToast(message: string) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 3000,
			position: 'bottom'
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});
		toast.present();
	}

	/*cartPage() {
		let modal = this.modalCtrl.create(CartPage);
		modal.onDidDismiss(() => {
			this.cartTotal = Number(this.global.getCartItemsCount());
		});
		modal.present();
	}*/

	cartPage() {
		this.navCtrl.push('CartPage');
	}

	detailsmodal(desc){
		console.log(desc);
		let modal = this.modalCtrl.create('AboutdescPage', {data: desc});
		modal.onDidDismiss(() => {
		});
		modal.present();
	}

	policymodal(){
		let modal = this.modalCtrl.create('PolicyPage');
		modal.onDidDismiss(() => {
		});
		modal.present();
	}

	sizechartpage()
	{
		let modal = this.modalCtrl.create('SizechartPage');
		modal.onDidDismiss(() => {
		});
		modal.present();
	}

	searchPage() {
		let modal = this.modalCtrl.create('SearchPage');
		modal.present();
	}

	addtowishlist(pro, index){
		if(this.wishcount!=0)
	    { 
	      this.wishlistproducts= JSON.parse(localStorage.getItem('wishlistproducts'));
	    }
	    this.wishlistproducts = this.wishlistproducts || [];
	    /*push into an array & set into localstorage*/
	    if (this.wishlistproducts.some(e => e.id === pro.id)) {
	    	this.showToast('updated to Wishlist');
	    	this.wishcolor = '#fd1d16';
		 	let pid = this.wishlistproducts.map(obj => obj.id).indexOf(pro.id);
			this.wishlistproducts.splice(pid, 1);
			this.product.wishlist = false;
		}
		else {
			this.showToast('Added to Wishlist');
			this.wishcolor = '#999';
			this.wishlistproducts.push(pro);	
			this.product.wishlist = true;
		}
	    localStorage.setItem('wishlistproducts', JSON.stringify(this.wishlistproducts));
	    this.wishcount = this.wishlistproducts.length;
	}


	itemdetailPage(pro) {
		this.navCtrl.push('ItemdetailPage', { pro: pro, pros: this.productsResponse });
	}

	/*kk code*/
	changeproduct(index){
		this.variationSelected = index;
		this.pid=index;
	    console.log(this.pid);
	}
	/*end*/

	delrecent()
	{
		localStorage.setItem('recentproducts', '[]');
		this.recent = [];
	}

	/*share product*/
	  shareProduct(product)
	  {
	  	//var link = 'https://play.google.com/store/apps/details?id=com.prpwebs.kharitatsandesh';
    	let link = 'https://play.google.com/store/apps/';
    	let description = product.name+' - Download App to read more ';
	    let image = 'https://prptests.com/london/wp-content/uploads/2019/05/logo.png';
	    if(product && product.images.length>0)
	    {
	    	let image = product.images[0].src;
	    }
	    this.socialSharing.shareViaWhatsApp(description, image, link).then(() => {
	      // Success!
	    }).catch(() => {
	      // Error!
	    });
	  }


}
