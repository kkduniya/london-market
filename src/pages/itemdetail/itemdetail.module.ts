import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { ItemdetailPage } from './itemdetail';
import { IonicImageLoader } from 'ionic-image-loader';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [ItemdetailPage],
  imports: [
    IonicPageModule.forChild(ItemdetailPage),
    IonicImageLoader,
    IonicImageViewerModule,
  ]
})
export class ItemdetailPageModule {}
