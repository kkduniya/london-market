import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-phonenumber ',
  templateUrl: 'phonenumber.html'
})
export class PhonenumberPage {

  constructor(public navCtrl: NavController) {

  }
    homePage(){
    this.navCtrl.push('HomePage');
    }
    passwordPage(){
    this.navCtrl.push('PasswordPage');
    }
}
