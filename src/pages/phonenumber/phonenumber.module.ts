import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { PhonenumberPage } from './phonenumber';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [PhonenumberPage],
  imports: [
    IonicPageModule.forChild(PhonenumberPage),
    IonicImageLoader
  ]
})
export class PhonenumberPageModule {}
