import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FocusPage } from './focus';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    FocusPage,
  ],
  imports: [
    IonicPageModule.forChild(FocusPage),
    IonicImageLoader
  ],
})
export class FocusPageModule {}
