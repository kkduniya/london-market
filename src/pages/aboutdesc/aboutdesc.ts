import { Component } from '@angular/core';
import { NavController, IonicPage,IonicPageModule, NavParams, ViewController, ToastController, App } from 'ionic-angular';

/**
 * Generated class for the AboutdescPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-aboutdesc',
  templateUrl: 'aboutdesc.html',
})
export class AboutdescPage {
	private description:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  	this.description = navParams.get('data');
    console.log(this.description);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutdescPage');
  }

  dismiss() {
		this.viewCtrl.dismiss();
	}

}
