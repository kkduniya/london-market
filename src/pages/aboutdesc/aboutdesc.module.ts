import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { AboutdescPage } from './aboutdesc';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [AboutdescPage],
  imports: [
    IonicPageModule.forChild(AboutdescPage),
    IonicImageLoader
  ]
})
export class AboutdescPageModule {}
