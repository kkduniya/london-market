import { Component } from '@angular/core';
import { NavController ,IonicPage,IonicPageModule } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-review ',
  templateUrl: 'review.html'
})
export class ReviewPage {

  constructor(public navCtrl: NavController) {

  }

}
