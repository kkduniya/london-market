import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { ReviewPage } from './review';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [ReviewPage],
  imports: [
    IonicPageModule.forChild(ReviewPage),
    IonicImageLoader
  ]
})
export class ReviewPageModule {}
