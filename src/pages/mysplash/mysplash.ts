import { Component } from '@angular/core';
import { NavController, IonicPage, IonicPageModule, Events } from 'ionic-angular';
import { Constants } from '../../models/constants.models';
import { Category } from '../../models/category.models';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Banner } from '../../models/banner.models';
import { Subscription } from '../../../node_modules/rxjs/Subscription';

@IonicPage()
@Component({
  selector: 'page-mysplash',
  templateUrl: 'mysplash.html'
})
export class MySplashPage {
 private banners = new Array<Banner>();
 private banner: any;
  constructor(private events: Events, public navCtrl: NavController, private service: WordpressClient) {
    this.loadBanners();
    let categories: Array<Category> = JSON.parse(window.localStorage.getItem(Constants.PRODUCT_CATEGORIES));
    if (categories) {
      setTimeout(() => {
        this.navCtrl.setRoot('HomePage');
      }, 2000);
    } else {
      events.subscribe('category:setup', () => {
        this.navCtrl.setRoot('HomePage');
      });
    }
  }

loadBanners() {
    let subscription: Subscription = this.service.banners(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {
      let banners: Array<Banner> = data;
      this.banners = banners;
      window.localStorage.setItem('banners', JSON.stringify(this.banners));
    }, err => {
    });
}


}
