import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { MySplashPage } from './mysplash';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [MySplashPage],
  imports: [
    IonicPageModule.forChild(MySplashPage),
    IonicImageLoader
  ]
})
export class MySplashPageModule {}
