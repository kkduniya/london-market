import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, NavParams, Loading, LoadingController, ToastController} from 'ionic-angular';
import { Global } from '../../providers/global';
import { WordpressClient } from '../../providers/wordpress-client.service';
import { Constants } from "../../models/constants.models";
import { Subscription } from "rxjs/Subscription";
import { Product } from "../../models/product.models";
import { Currency } from "../../models/currency.models";
import { Category } from "../../models/category.models";
import { Image } from '../../models/image.models';

/**
 * Generated class for the DesignerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-designer',
  templateUrl: 'designer.html',
  providers: [WordpressClient, Global]
})
export class DesignerPage {
 private subscriptions: Array<Subscription> = [];
 private productsAll = new Array<Array<Product>>();
 private productsResponse = new Array<Product>();
 private currencyIcon: string;
 private currencyText: string;
 private loading: Loading;
 private loadingShown: Boolean = false;
 private product: Product;
 private pageno: number = 1;
 private designcat: number = 45;
  constructor(public navCtrl: NavController, public navParams: NavParams, private service: WordpressClient,private toastCtrl: ToastController, private global: Global, private loadingCtrl: LoadingController) {
  	this.presentLoading('Loading..');
  	let currency: Currency = JSON.parse(window.localStorage.getItem(Constants.CURRENCY));
		if (currency) {
			this.currencyText = currency.value;
			let iconText = currency.options[currency.value];
			this.currencyIcon = iconText.substring(iconText.lastIndexOf('(') + 1, iconText.length - 1);
		}
  	this.productsbycategory();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DesignerPage');
  }

  productsbycategory(){ 
  	let subscription: Subscription = this.service.productsByCategory(window.localStorage.getItem(Constants.ADMIN_API_KEY),String(this.designcat),String(this.pageno)).subscribe(data => {
  		this.dismissLoading();
		let products: Array<Product> = data;
			this.dismissLoading();
			this.productsResponse = products;
			let proSplit = new Array<Product>();
			for (let pro of products) {
				if (!pro.purchasable || pro.type == 'grouped' || pro.type == 'external')
					continue;
				if (proSplit.length == 2) {
					this.productsAll.push(proSplit);
					proSplit = new Array<Product>();
				}
				if (!pro.sale_price) {
					pro.sale_price = pro.regular_price;
				}
				if (this.currencyIcon) {
					pro.regular_price_html = this.currencyIcon + ' ' + pro.regular_price;
					pro.sale_price_html = this.currencyIcon + ' ' + pro.sale_price;
				} else if (this.currencyText) {
					pro.regular_price_html = this.currencyText + ' ' + pro.regular_price;
					pro.sale_price_html = this.currencyText + ' ' + pro.sale_price;
				}
				pro.favorite = this.global.isFavorite(pro);
				proSplit.push(pro);
			}
			if (proSplit.length > 0) {
				this.productsAll.push(proSplit);
			}
			this.productsAll = this.productsAll;
		}, err => { 
			this.dismissLoading();
		});
		this.subscriptions.push(subscription);
	}

	doInfinite(infiniteScroll: any) {
		this.pageno++;
		let subscription: Subscription = this.service.productsByCategory(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.designcat), String(this.pageno)).subscribe(data => {
			this.dismissLoading();
			let products: Array<Product> = data;
			console.log(data);
			this.productsResponse = products;
			console.log(this.productsResponse);
			let proSplit = new Array<Product>();
			for (let pro of products) {
				if (!pro.purchasable || pro.type == 'grouped' || pro.type == 'external')
					continue;
				if (proSplit.length == 2) {
					this.productsAll.push(proSplit);
					proSplit = new Array<Product>();
				}
				if (!pro.sale_price) {
					pro.sale_price = pro.regular_price;
				}
				if (this.currencyIcon) {
					pro.regular_price_html = this.currencyIcon + ' ' + pro.regular_price;
					pro.sale_price_html = this.currencyIcon + ' ' + pro.sale_price;
				} else if (this.currencyText) {
					pro.regular_price_html = this.currencyText + ' ' + pro.regular_price;
					pro.sale_price_html = this.currencyText + ' ' + pro.sale_price;
				}
				pro.favorite = this.global.isFavorite(pro);
				proSplit.push(pro);
			}
			if (proSplit.length > 0) {
				this.productsAll.push(proSplit);
			}
			infiniteScroll.complete();
		}, err => {
			infiniteScroll.complete();
			this.dismissLoading();
		});
		this.subscriptions.push(subscription);
	}


  itemdetailPage(pro) {
		this.navCtrl.push('ItemdetailPage', { pro: pro, pros: pro });
	}

	private presentLoading(message: string) {
	this.loading = this.loadingCtrl.create({
		content: message
	});

	this.loading.onDidDismiss(() => { });

	this.loading.present();
	this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

}
