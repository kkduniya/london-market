import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { DesignerPage } from './designer';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [DesignerPage],
  imports: [
    IonicPageModule.forChild(DesignerPage),
    IonicImageLoader
  ]
})
export class DesignerPageModule {}
