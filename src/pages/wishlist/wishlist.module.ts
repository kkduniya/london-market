import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { WishlistPage } from './wishlist';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [WishlistPage],
  imports: [
    IonicPageModule.forChild(WishlistPage),
    IonicImageLoader
  ]
})
export class WishlistPageModule {}
