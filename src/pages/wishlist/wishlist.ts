import { Component } from '@angular/core';
import { NavController, ModalController,IonicPage,IonicPageModule} from 'ionic-angular';
import { Global } from '../../providers/global';
import { Product } from "../../models/product.models";

@IonicPage()
@Component({
	selector: 'page-wishlist ',
	templateUrl: 'wishlist.html',
	providers: [Global]
})

export class WishlistPage {
	private favorites: Array<Product>;
	private wishlistproducts:any=[];
	constructor(public navCtrl: NavController, public modalCtrl: ModalController, private global: Global) {
		this.favorites = global.getFavorites();
		this.wishlistproducts = JSON.parse(localStorage.getItem('wishlistproducts'));
		console.log(this.wishlistproducts);
		if(this.wishlistproducts==null){
			this.wishlistproducts = [];
		}
		console.log(this.wishlistproducts);
	}

	cartPage() {
		let modal = this.modalCtrl.create('CartPage');
		modal.present();
	}

	itemdetailPage(pro) {
		this.navCtrl.push('ItemdetailPage', { pro: pro});
	}

	removeFavorite(id){
		console.log(this.wishlistproducts.length);
		//let items = JSON.parse(localStorage.getItem('wishlistproducts'));
		for (var i =0; i<= this.wishlistproducts.length; i++) {
		    var item = this.wishlistproducts[i];
		    if (item.id == id) {
		        this.wishlistproducts.splice(i, 1);
		    }
		}
		console.log(this.wishlistproducts);
		if(this.wishlistproducts.length==0)
		localStorage.setItem("wishlistproducts", '[]');
		else
		localStorage.setItem("wishlistproducts", this.wishlistproducts);
	}

	goBack() {
		this.navCtrl.push('HomePage');
	}
	
}
