import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { VegetablePage } from './vegetable';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [VegetablePage],
  imports: [
    IonicPageModule.forChild(VegetablePage),
    IonicImageLoader
  ]
})
export class VegetablePageModule {}
