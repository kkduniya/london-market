import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { ShirtsPage } from './shirts';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [ShirtsPage],
  imports: [
    IonicPageModule.forChild(ShirtsPage),
    IonicImageLoader
  ]
})
export class ShirtsPageModule {}
