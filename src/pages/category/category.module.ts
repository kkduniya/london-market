import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { CategoryPage } from './category';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [CategoryPage],
  imports: [
    IonicPageModule.forChild(CategoryPage),
    IonicImageLoader
  ]
})
export class CategoryPageModule {}
