import { Component } from '@angular/core';
import { NavController,IonicPage,IonicPageModule, ModalController, Loading, LoadingController, ToastController } from 'ionic-angular';
import { Category } from "../../models/category.models";
import { Constants } from "../../models/constants.models";
import { Subscription } from '../../../node_modules/rxjs/Subscription';
import { WordpressClient } from '../../providers/wordpress-client.service';

@IonicPage()
@Component({
	selector: 'page-category ',
	templateUrl: 'category.html',
	providers: [WordpressClient]

})
export class CategoryPage {
	private categoriesAllNew = new Array<Category>();
	private categoriesAll: Array<Array<Category>>;
	private catsToShow: Array<Category>;
	private subscriptions: Array<Subscription> = [];
	private pageCategory: number = 1;
	private firstTime: boolean;
	private refreshing: boolean;
	private loading: Loading;
 	private loadingShown: Boolean = false;

	constructor(public navCtrl: NavController, private toastCtrl: ToastController, private service: WordpressClient, public modalCtrl: ModalController, private loadingCtrl: LoadingController) {
		this.presentLoading('Loading..');
		 let categoriesAll: Array<Category> = JSON.parse(window.localStorage.getItem(Constants.PRODUCT_CATEGORIES));
		/*let categoriesAll: Array<Category>; */
		/*let subscription: Subscription = this.service.categories(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.pageCategory)).subscribe(data => {
			categoriesAll = data;
			console.log(categoriesAll);
		});*/

		this.firstTime = categoriesAll == null;
		this.setupCategories(categoriesAll);
		if (this.firstTime) {
			this.catdoRefresh();
		}

	}

	ionViewWillLeave() {
		this.subscriptions.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
	}

	catdoRefresh() {
		if (!this.refreshing) {
			this.refreshing = true;
			this.showToast('Refreshing...');
			this.refreshCategories();
		}
	}
	/*kk code refresh*/
	refresh(event) {
	    this.refreshCategories();
	    setTimeout(() => {
	      console.log('Async operation has ended');
	      event.complete();
	    }, 2000);
	 }

	refreshCategories() {
		let subscription: Subscription = this.service.categories(window.localStorage.getItem(Constants.ADMIN_API_KEY), String(this.pageCategory)).subscribe(data => {
			//this.dismissLoading();
			let categories: Array<Category> = data;
			if (categories.length == 0) {
				window.localStorage.setItem(Constants.PRODUCT_CATEGORIES, JSON.stringify(this.categoriesAllNew));
				console.log('categories setup success');
				this.setupCategories(this.categoriesAllNew);
				this.refreshing = false;
			} else {
				this.categoriesAllNew = this.categoriesAllNew.concat(categories);
				this.pageCategory++;
				this.refreshCategories();
			}
		}, err => {
			console.log('categories setup error');
		});
		this.subscriptions.push(subscription);
	}

	setupCategories(savedCats: Array<Category>) {
		if (savedCats && savedCats.length) {
			this.categoriesAll = new Array<Array<Category>>();
			let parentWithChild: Array<Category>;
			for (let catP of savedCats) {
				if (Number(catP.parent) == 0) {
					parentWithChild = new Array<Category>();
					for (let catC of savedCats) {
						if (Number(catP.id) == Number(catC.parent)) {
							parentWithChild.push(catC);
						}
					}
					parentWithChild.unshift(catP);
					this.categoriesAll.push(parentWithChild);
				}
			}
		}
		this.dismissLoading();
	}

	showCats(cats) {
		console.log(cats);
		if (cats && cats.length > 1 ){
			this.navCtrl.push('SubcategoryPage', { cat: cats, parent: 'catwithp' });
		}
		else{
			this.shirtsPage(cats[0]);
		}
	}

	shirtsPage(cat: Category) {
		if (Number(cat.id) == Number(254)) {
			this.navCtrl.push('VendorsPage');
		}
		else{
			this.navCtrl.push('ShirtsPage', { cat: cat });
		}
	}

	searchPage() {
	    this.navCtrl.push('SearchPage');
	    // let modal = this.modalCtrl.create(SearchPage);
	    // modal.present();
  	}

	cartPage() {
		let modal = this.modalCtrl.create('CartPage');
		modal.present();
	}

	showToast(message: string) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 3000,
			position: 'bottom'
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});
		toast.present();
	}

	private presentLoading(message: string) {
		this.loading = this.loadingCtrl.create({
			content: message
		});
		this.loading.onDidDismiss(() => { });
		this.loading.present();
		this.loadingShown = true;
	}

	private dismissLoading() {
		if (this.loadingShown) {
			this.loadingShown = false;
			this.loading.dismiss();
		}
	}

	goBack() {
		this.navCtrl.push('HomePage');
	}

}