import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { CodePage } from './code';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [CodePage],
  imports: [
    IonicPageModule.forChild(CodePage),
    IonicImageLoader
  ]
})
export class CodePageModule {}
