import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Globalization } from '@ionic-native/globalization/ngx';
import { Device } from '@ionic-native/device/ngx';
import { APP_CONFIG, BaseAppConfig } from "./app.config";
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AddressSelectPage } from '../pages/addressselect/addressselect';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { IonicImageLoader } from 'ionic-image-loader';
import { AppUpdate } from '@ionic-native/app-update/ngx';
//import { CodePush } from '@ionic-native/code-push/ngx';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    FilterPipeModule,
    HttpClientModule,
    IonicImageViewerModule,
    IonicImageLoader.forRoot(),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    Device,
    StatusBar,
    SplashScreen,
    InAppBrowser,
    SocialSharing,
    AppUpdate,
    Globalization,
    Camera,
    OneSignal,
    //CodePush,
    { provide: APP_CONFIG, useValue: BaseAppConfig },
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }