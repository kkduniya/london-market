import { InjectionToken } from "@angular/core";

export let APP_CONFIG = new InjectionToken<AppConfig>("app.config");

export interface FirebaseConfig {
	apiKey: string,
	authDomain: string,
	databaseURL: string,
	projectId: string,
	storageBucket: string,
	messagingSenderId: string,
	webApplicationId: string
}

export interface AppConfig {
	appName: string;
	apiBase: string;
	hosturl: string;
	isDemo: boolean;
	consumerKey: string;
	consumerSecret: string;
	adminUsername: string;
	adminPassword: string;
	oneSignalAppId: string;
	oneSignalGPSenderId: string;
	paypalSandbox: string;
	paypalProduction: string;
	payuSalt: string;
	payuKey: string;
	availableLanguages: Array<any>;
	firebaseConfig: FirebaseConfig;
	razorpaykey: string;
	razorpaysecret: string;
}

export const BaseAppConfig: AppConfig = {
	appName: "London Market",
	apiBase: "https://londonmarket.in/wp-json/",
	hosturl: "https://londonmarket.in/",
	isDemo: false,
	consumerKey: "ck_50be2494a98b288a5cc133a679ed06909eb657e9",
	consumerSecret: "cs_db862a64972de6c9fd2db789ed9f883c3be978b3",
	adminUsername: "londonmarket",
	adminPassword: "bjnr@DEC12",
	oneSignalAppId: "2f852387-5f6d-4e00-ab21-a71b2bbe3b5b",
	oneSignalGPSenderId: "",
	paypalSandbox: "",
	paypalProduction: "",
	payuSalt: "cqn3GQx90Q",
	payuKey: "CLtIqxhZ",
	razorpaykey:"rzp_live_hpM3vq6N79YxMN",
	razorpaysecret: 'KVljk5n7uDDYTRmmrSuZMTB9',
	availableLanguages: [{
		code: 'en',
		name: 'English'
	}, {
		code: 'ar',
		name: 'Arabic'
	}, {
		code: 'es',
		name: 'Spanish'
	}, {
		code: 'pt',
		name: 'Portuguese'
	}],
	firebaseConfig: {
		webApplicationId: "579962439806-bmpv7lo888ef1ja3nf6q9i3k95uhjvhh.apps.googleusercontent.com",
		apiKey: "AIzaSyBx6BKSUiI2Rj3V8tUM_abDV4YLoREUXFM",
   		authDomain: "london-luxury-market.firebaseapp.com",
   	    databaseURL: "https://london-luxury-market.firebaseio.com",
        projectId: "london-luxury-market",
        storageBucket: "london-luxury-market.appspot.com",
        messagingSenderId: "579962439806"
	}
};


