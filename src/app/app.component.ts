import { Component, ViewChild, Inject } from '@angular/core';
import { Nav, Platform, AlertController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Device } from '@ionic-native/device/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { WordpressClient } from '../providers/wordpress-client.service';
import { Subscription } from "rxjs/Subscription";
import { AuthResponse } from "../models/auth-response.models";
import { AuthCredential } from "../models/auth-credential.models";
import { Constants } from "../models/constants.models";
import { Category } from "../models/category.models";
import { PaymentGateway } from "../models/payment-gateway.models";
import { ShippingLine } from "../models/shipping-line.models";
import { UserResponse } from "../models/user-response.models";
import { Currency } from "../models/currency.models";
import { Banner } from '../models/banner.models';
import { APP_CONFIG, AppConfig } from './app.config';
import firebase from 'firebase';
import { TimeSlot } from '../models/time-slot.models';
import { ShippingZone } from '../models/shipping-zone.models';
import { ShippingZoneLocation } from '../models/shipping-zone-location.models';
import { AppUpdate } from '@ionic-native/app-update/ngx';
//import { CodePush } from '@ionic-native/code-push/ngx';

@Component({
	templateUrl: 'app.html',
	providers: [WordpressClient]
})
export class MyApp {
	deviceModel = "";
	@ViewChild(Nav) nav: Nav;
	private subscriptions: Array<Subscription> = [];

	rootPage: any = 'HomePage';
	pages: Array<{ title: string, component: any }>;
	pageCategory: number = 1;
	categoriesAll = new Array<Category>();
	user: UserResponse;
	private showedAlert: Boolean = false;
	private confirmAlert: any;
	private banners = new Array<Banner>();

	constructor(@Inject(APP_CONFIG) private config: AppConfig, private device: Device, private events: Events, private alertCtrl: AlertController, private service: WordpressClient, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private oneSignal: OneSignal, private appUpdate: AppUpdate ) {
		splashScreen.hide();
		/*this.loadSlots();*/
		let superAuth = "";
		if (config.apiBase && config.apiBase.startsWith('https') && config.consumerKey && config.consumerKey.length && config.consumerSecret && config.consumerSecret.length) {
			superAuth = ("Basic " + btoa(config.consumerKey + ":" + config.consumerSecret));
			window.localStorage.setItem(Constants.ADMIN_API_KEY, superAuth);
			this.onSuperAuthSetup(superAuth);
		} else if (config.apiBase && config.apiBase.startsWith('http:') && config.adminUsername && config.adminUsername.length && config.adminPassword && config.adminPassword.length) {
			let subscription: Subscription = service.getAuthToken(new AuthCredential(config.adminUsername, config.adminPassword)).subscribe(data => {
				let authResponse: AuthResponse = data;
				superAuth = ("Bearer " + authResponse.token);
				window.localStorage.setItem(Constants.ADMIN_API_KEY, superAuth);
				this.onSuperAuthSetup(superAuth);
			}, err => {
				console.log('auth setup error');
			});
			this.subscriptions.push(subscription);
		} else {
			console.log('auth setup error');
		}

		this.user = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));
		this.initializeApp();
		this.listenToLoginEvents();
		
	}

	loadSlots() {
		let subscription: Subscription = this.service.timeslots().subscribe(data => {
			let slots: Array<TimeSlot> = data;
			window.localStorage.setItem(Constants.TIME_SLOTS, JSON.stringify(slots));
			console.log('timeslot setup success');
		}, err => {
			console.log('timeslot setup error');
		});
		this.subscriptions.push(subscription);
	}

	listenToLoginEvents() {
		this.events.subscribe('user:login', () => {
			this.user = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));
		});
	}

	onSuperAuthSetup(superAuth) {
		console.log('auth setup success: ' + superAuth);
		this.loadParentCategories();
		this.loadCategories();
		this.loadCurrency();
		this.loadPaymentGateways();
		this.loadShipping();
		this.loadBanners();
	}

	loadCurrency() {
		let savedCurrency: Currency = JSON.parse(window.localStorage.getItem(Constants.CURRENCY));
		if (!savedCurrency) {
			let subscription: Subscription = this.service.currencies(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {
				let currency: Currency = data;
				window.localStorage.setItem(Constants.CURRENCY, JSON.stringify(currency));
				console.log('currency setup success');
			}, err => {
				console.log('currency setup error');
			});
			this.subscriptions.push(subscription);
		}
	}

	loadShipping() {
		let subscription2: Subscription = this.service.shippingZones(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {
			let shippingZones: Array<ShippingZone> = data;
			console.log('shippingZones', shippingZones);
			let shippingZoneLocations = new Array<ShippingZoneLocation>();
			let timesReturned = 0;
			for (let sz of shippingZones) {
				let subscription3: Subscription = this.service.shippingZoneLocations(window.localStorage.getItem(Constants.ADMIN_API_KEY), sz.id).subscribe(data => {
					shippingZoneLocations = shippingZoneLocations.concat(data);
					timesReturned = timesReturned + 1;
					if (timesReturned == shippingZones.length) {
						window.localStorage.setItem(Constants.SHIPPING_ZONE_LOCATIONS, JSON.stringify(shippingZoneLocations));
						console.log('shippingZoneLocations', shippingZoneLocations);
						console.log('shippingZoneLocations setup done');
					}
				}, err => {
					timesReturned = timesReturned + 1;
					if (timesReturned == shippingZones.length) {
						console.log('ErrShippingZoneLocation', err);
					}
				});
				this.subscriptions.push(subscription3);
			}
		}, err => {
			console.log('ErrShippingZone', err);
		});
		this.subscriptions.push(subscription2);
	}

	loadPaymentGateways() {
		let subscription: Subscription = this.service.paymentGateways(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {
			let paymentGateway: Array<PaymentGateway> = data;
			window.localStorage.setItem(Constants.PAYMENT_GATEWAYS, JSON.stringify(paymentGateway));
			console.log('payment-gateway setup success');
		}, err => {
			console.log('categories setup error');
		});
		this.subscriptions.push(subscription);
	}
	/*parents categories*/
	loadParentCategories() {
		let subscription: Subscription = this.service.categoriesParent(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {
			let categories: Array<Category> = data;
			window.localStorage.setItem(Constants.PRODUCT_CATEGORIES_PARENT, JSON.stringify(categories));
			console.log('Parent categories setup success');
			this.events.publish('category:setup');
		}, err => {
			console.log('ErrCategories', err);
		});
		this.subscriptions.push(subscription);
	}

	/*categories*/
	loadCategories() {
		let subscription: Subscription = this.service.categories(window.localStorage.getItem(Constants.ADMIN_API_KEY),"1").subscribe(data => {
			let categories: Array<Category> = data;
			window.localStorage.setItem(Constants.PRODUCT_CATEGORIES, JSON.stringify(categories));
			console.log('categories setup success');
			this.events.publish('category:setup');
		}, err => {
			console.log('ErrCategories', err);
		});
		this.subscriptions.push(subscription);
	}

	loadBanners() {
	    let subscription: Subscription = this.service.banners(window.localStorage.getItem(Constants.ADMIN_API_KEY)).subscribe(data => {
	      let banners: Array<Banner> = data;
	      this.banners = banners;
	      window.localStorage.setItem('banners', JSON.stringify(this.banners));
	    }, err => {
	    });
	}

	initializeApp() {
		this.platform.ready().then(() => {

			/*code push updateion*/
			// as syncStatus will contain the current state of the update
				/*this.codePush.sync().subscribe((syncStatus) => console.log(syncStatus));
				const downloadProgress = (progress) => { console.log(`Downloaded ${progress.receivedBytes} of ${progress.totalBytes}`); }
				this.codePush.sync({}, downloadProgress).subscribe((syncStatus) => console.log(syncStatus));*/
			/*end*/
			
			this.statusBar.styleDefault();
			this.splashScreen.hide();

			firebase.initializeApp({
				apiKey: this.config.firebaseConfig.apiKey,
				authDomain: this.config.firebaseConfig.authDomain,
				databaseURL: this.config.firebaseConfig.databaseURL,
				projectId: this.config.firebaseConfig.projectId,
				storageBucket: this.config.firebaseConfig.storageBucket,
				messagingSenderId: this.config.firebaseConfig.messagingSenderId
			});

			try {
				if (this.device.model) {
					this.deviceModel = this.device.model.replace(/\s/g, '').replace(',', '').toLowerCase();
					// iphone model nos. https://gist.github.com/adamawolf/3048717
					if (this.deviceModel.indexOf("iphone103") != -1 || this.deviceModel.indexOf("iphone106") != -1 || this.deviceModel.indexOf("iphonex") != -1) {
						this.deviceModel = "iphonex";
					}
				}
			} catch (exception) {

			}

			if (this.platform.is('cordova')) {
				this.initOneSignal();
				/*ionic app update*/
				const updateUrl = 'https://londonmarket.in/update.xml';
	   			this.appUpdate.checkAppUpdate(updateUrl).then(update => {
		          alert("Update Status:  "+update.msg);
		        }).catch(error=>{
		          alert("Error: "+error.msg);
		        });
			} 

		/*Before Exit from the App*/
			this.platform.registerBackButtonAction(() => {
	            if (this.nav.length() == 1) {
	                if (!this.showedAlert) {
	                    this.confirmExitApp();
	                } else {
	                    this.showedAlert = false;
	                    this.confirmAlert.dismiss();
	                }
	            }
	            this.nav.pop();
         	});
		/*End*/
		});
	}

	confirmExitApp() {
	    this.showedAlert = true;
	    this.confirmAlert = this.alertCtrl.create({
	        title: "Exit",
	        message: "Do you want to exit?",
	        buttons: [
	            {
	                text: 'Cancel',
	                handler: () => {
	                    this.showedAlert = false;
	                    return;
	                }
	            },
	            {
	                text: 'Exit',
	                handler: () => {
	                    this.platform.exitApp();
	                }
	            }
	        ]
	    });
	    this.confirmAlert.present();
	}


	initOneSignal() {
		if (this.config.oneSignalAppId && this.config.oneSignalAppId.length && this.config.oneSignalGPSenderId && this.config.oneSignalGPSenderId.length) {
			this.oneSignal.startInit(this.config.oneSignalAppId, this.config.oneSignalGPSenderId);
			this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
			this.oneSignal.handleNotificationReceived().subscribe((data) => {
				// do something when notification is received
				console.log(data);
			});
			this.oneSignal.handleNotificationOpened().subscribe((data) => {
				if (data.notification.payload
					&& data.notification.payload.additionalData) {
					this.myorder_1Page();
				}
			});
			this.oneSignal.endInit();

			this.oneSignal.getIds().then((id) => {
				if (id.userId) {
					window.localStorage.setItem(Constants.ONESIGNAL_PLAYER_ID, id.userId.toString());
				}
			});
		}
	}

	actionNavHeader() {
		if (this.user) {
			if (this.nav.getActive().name != 'My_accountPage')
				this.nav.setRoot('My_accountPage');
		} else {
			if (this.nav.getActive().name != 'LoginPage')
				this.nav.push('LoginPage');
		}
	}

	addressPage() {
		if (this.nav.getActive().name != 'AddressSelectPage')
			this.nav.push('AddressSelectPage');
	}

	myorder_1Page() {
		if (this.nav.getActive().name != 'Myorder_2Page')
			this.nav.push('Myorder_2Page');
			this.nav
	}

	myorder_2Page() {
		if (this.nav.getActive().name != 'Myorder_2Page')
			this.nav.push('Myorder_2Page');
	}

	my_accountPage() {
		if (this.nav.getActive().name != 'My_accountPage')
			this.nav.push('My_accountPage');
	}

	categoryPage() {
		if (this.nav.getActive().name != 'CategoryPage')
			this.nav.push('CategoryPage');
	}

	homePage() {
		if (this.nav.getActive().name != 'HomePage')
			this.nav.setRoot('HomePage');
	}

	reviewPage() {
		if (this.nav.getActive().name != 'ReviewPage')
			this.nav.setRoot('ReviewPage');
	}

	wishlistPage() {
		if (this.nav.getActive().name != 'WishlistPage')
			this.nav.push('WishlistPage');
	}

	cartPage() {
		if (this.nav.getActive().name != 'CartPage')
			this.nav.push('CartPage');
	}

	helpPage() {
		if (this.nav.getActive().name != 'HelpPage')
			this.nav.push('HelpPage');
	}

	loginPage() {
		if (this.nav.getActive().name != 'LoginPage')
			this.nav.push('LoginPage');
	}

	allbrand() {
		if (this.nav.getActive().name != 'AllbrandPage')
			this.nav.push('AllbrandPage');
	}

	justinpage() {
		if (this.nav.getActive().name != 'JustinPage')
			this.nav.push('JustinPage');
	}

	giftPage() {
		if (this.nav.getActive().name != 'GiftingPage')
			this.nav.push('GiftingPage');
	}

	designers() {
		if (this.nav.getActive().name != 'DesignerPage')
			this.nav.push('DesignerPage');
	}

	support() {
		if (this.nav.getActive().name != 'SupportPage')
			this.nav.push('SupportPage');
	}

	appointment() {
		if (this.nav.getActive().name != 'AppointmentPage')
			this.nav.push('AppointmentPage');
	}

	exhibition() {
		if (this.nav.getActive().name != 'ExhibitionPage')
			this.nav.push('ExhibitionPage');
	}

	tagspage(id) {
		if (this.nav.getActive().name != 'TagproductPage')
			this.nav.push('TagproductPage',{tag_id:id, order_msg:id});
	}

	categoriesPage() {
		if (this.nav.getActive().name != 'CategoryPage')
			this.nav.push('CategoryPage');
	}

	phonenumberPage() {
		let alert = this.alertCtrl.create({
			title: 'Logout',
			message: 'Are you sure you want to logout?',
			buttons: [{
				text: 'No',
				role: 'cancel',
				handler: () => {
					console.log('Cancel clicked');
				}
			},
			{
				text: 'Yes',
				handler: () => {
					this.user = null;
					window.localStorage.setItem(Constants.USER_KEY, null);
					this.homePage();
				}
			}]
		});
		alert.present();
	}
}
