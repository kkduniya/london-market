import {Serializable} from "./serializalble.interface";

export class Feedback {
	name: string;
	email: string;
	mobile: string;
	description: string;
}