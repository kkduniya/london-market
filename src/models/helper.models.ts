export class Helper {
    static extractUserIdFromError(err) {
        if (err && err.error && err.error.text) {
            let errText = String(err.error.text);
            let usersIndex = errText.indexOf("/users/");
            if (usersIndex != -1) {
                errText = errText.substring(usersIndex + "/users/".length, errText.length);
                let qIndex = errText.indexOf("\"");
                if (qIndex != -1) {
                    return Number(errText.substring(0, qIndex));
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
    static extractOrderIdFromError(err) {
        if (err && err.error && err.error.text) {
            let errText = String(err.error.text);
            let usersIndex = errText.indexOf("/orders/");
            if (usersIndex != -1) {
                errText = errText.substring(usersIndex + "/orders/".length, errText.length);
                let qIndex = errText.indexOf("\"");
                if (qIndex != -1) {
                    return Number(errText.substring(0, qIndex));
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
    static tofixed(toFix: number) {
        let fixedO = toFix.toFixed(2);
        return fixedO.endsWith(".00") ? fixedO.substring(0, fixedO.length - 3) : fixedO;
    }
}