import { Serializable } from "./serializalble.interface";

export class StripeResponse {
    code: number;
    message: string;
}