import {Serializable} from "./serializalble.interface";

export class ShippingZone {
	id: string;
    name: string;
    order: number;
}